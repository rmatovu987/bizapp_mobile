package com.falcontechnology.signup;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.components.SpanLabel;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import com.falcontechnology.auth.LoginForm;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;

/**
 *
 * @author falcon
 */
public class SignUpForm extends Form {

    FormFiled country = new FormFiled(Boolean.FALSE, "Country*", Boolean.TRUE, Display.PICKER_TYPE_STRINGS);
    FormFiled currency = new FormFiled(Boolean.FALSE, "Currency*", Boolean.TRUE, Display.PICKER_TYPE_STRINGS);
    FormFiled businessname = new FormFiled(Boolean.FALSE, "Business Name*");
    FormFiled firstName = new FormFiled(Boolean.FALSE, "First name*");
    FormFiled lastName = new FormFiled(Boolean.FALSE, "Last name*");
    FormFiled email = new FormFiled(Boolean.FALSE, "Email Address*");
    FormFiled phoneno = new FormFiled(Boolean.FALSE, "Telephone number*");
    FormFiled password = new FormFiled(Boolean.FALSE, "Password*");
    FormFiled confirmpassword = new FormFiled(Boolean.FALSE, "Confirm Password*");

    RichComponents rich = new RichComponents();

    public SignUpForm(Resources theme) {

        super(new BorderLayout());
        setUIID("Form-Background");

        Toolbar mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        setToolbar(mainToolbar);

        Container TittleContainer = BoxLayout.encloseX();
        TittleContainer.setUIID("Container");
        Label toobarLabel = new Label("Sign Up", "Toolbar-HeaderLabel");
        mainToolbar.setTitleComponent(TittleContainer);
        mainToolbar.setTitleCentered(true);
        TittleContainer.add(toobarLabel);

        Form previous = Display.getInstance().getCurrent();
        mainToolbar.setBackCommand("", e -> new LoginForm(theme).show());

        loadCountries();

        email.setIsEmail(Boolean.TRUE);

        phoneno.setIsPhone(Boolean.TRUE);

        password.setIsPassword(Boolean.TRUE);
        confirmpassword.setIsPassword(Boolean.TRUE);

        Button next = rich.blueButton("Create Account");

        Button signIn = rich.wordButton(false, "Sign In");
        signIn.addActionListener(e -> previous.showBack());

        Label alreadHaveAnAccount = new Label("Already have an account?", "Dont-Have-Account-Label");
        next.requestFocus();

        Container group = new Container(BoxLayout.y());
        group.setUIID("Container-Background");
        group.addAll(
                businessname.getFormField(),
                country.getFormField(),
                currency.getFormField(),
                firstName.getFormField(),
                lastName.getFormField(),
                email.getFormField(),
                phoneno.getFormField(),
                password.getFormField(),
                confirmpassword.getFormField(),
                createLineSeparator(),
                next,
                FlowLayout.encloseCenterMiddleByRow(alreadHaveAnAccount, signIn));
        group.setScrollableY(true);

        add(BorderLayout.NORTH, group);

        next.addActionListener(e -> {
            try {

                if (!"".equals(country.getContent()) && !"".equals(currency.getContent())
                        && !"".equals(firstName.getContent()) && !"".equals(businessname.getContent())
                        && !"".equals(lastName.getContent()) && !"".equals(email.getContent())
                        && !"".equals(password.getContent()) && !"".equals(confirmpassword.getContent())
                        && !"".equals(phoneno.getContent())) {
                    JSONObject reqJson = new JSONObject();
                    reqJson.put("country", country.getContent());
                    reqJson.put("currency", currency.getContent());
                    reqJson.put("businessName", businessname.getContent());
                    reqJson.put("firstName", firstName.getContent());
                    reqJson.put("lastName", lastName.getContent());
                    reqJson.put("email", email.getContent());
                    reqJson.put("phoneNumber", phoneno.getContent());
                    reqJson.put("password", password.getContent());
                    reqJson.put("confirmPassword", confirmpassword.getContent());

                    Response<String> result = Rest.post(URLLinks.getMainBackend() + "auth/register")
                            .body(reqJson.toString())
                            .jsonContent()
                            .onErrorCodeJSON((errorData) -> {
                                if (errorData.getResponseCode() == 409) {
                                    Dialog dlg = rich.Dialog("Conflict Error");

                                    Button yes = new Button("Close");
                                    yes.addActionListener(ev -> {
                                        dlg.dispose();
                                        new LoginForm(theme).show();
                                    });

                                    dlg.add(new SpanLabel("User exists! Log into your account and go to settings and add another business!"));
                                    dlg.add(FlowLayout.encloseRight(yes));
                                    dlg.showPacked(BorderLayout.CENTER, true);

                                } else if (errorData.getResponseCode() == 501) {
                                    Dialog dlg = rich.Dialog("Password Error");

                                    Button yes = new Button("Close");
                                    yes.addActionListener(ev -> {
                                        dlg.dispose();
                                    });

                                    dlg.add(new SpanLabel("Passwords don't match! Make sure the passwords are the same."));
                                    dlg.add(FlowLayout.encloseRight(yes));
                                    dlg.showPacked(BorderLayout.CENTER, true);

                                }else if (errorData.getResponseCode() == 403) {
                                    Dialog dlg = rich.Dialog("Email Error");

                                    Button yes = new Button("Close");
                                    yes.addActionListener(ev -> {
                                        dlg.dispose();
                                    });

                                    dlg.add(new SpanLabel("Invalid email. Please enter a valid email address!"));
                                    dlg.add(FlowLayout.encloseRight(yes));
                                    dlg.showPacked(BorderLayout.CENTER, true);

                                }
                            })
                            .getAsString();

                    if (result.getResponseCode() == 200) {
                        rich.SuccessSnackbar(result.getResponseData());

                        new LoginForm(theme).show();
                    }
                } else {
                    rich.MandatoryDialog();
                }

            } catch (Exception ex) {
            }

        });

    }

    private void loadCountries() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "auth").acceptJson().contentType("application/json").getAsString();

            if (result.getResponseCode() == 200) {
                JSONArray data = new JSONArray(result.getResponseData());
                String[] countries = new String[data.length()];
                String[] currencies = new String[data.length()];

                for (int i = 0; i < data.length(); i++) {
                    JSONObject item = data.getJSONObject(i);
                    countries[i] = item.getString("country");
                    currencies[i] = item.getString("name");
                }
                country.setPickerData(countries);
                currency.setPickerData(currencies);
            }
        } catch (JSONException ex) {
        }
    }

    private Component createLineSeparator() {
        Label separator = new Label("", "Commons-Space");
        separator.setShowEvenIfBlank(true);
        return separator;
    }
}
