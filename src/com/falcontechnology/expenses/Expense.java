/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.expenses;

import com.codename1.ui.Image;

/**
 *
 * @author falcon
 */
public class Expense {
    private Long id;
    private String date;
    private String description;
    private double cost;
    private ExpenseType type;
    private Image photo;
    private String creator;

    public Expense() {
    }

    public Expense(Long id, String date, String description, double cost, ExpenseType type, Image photo) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.cost = cost;
        this.type = type;
        this.photo = photo;
    }
    
    public Expense(Long id, String date, String description, double cost, ExpenseType type) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.cost = cost;
        this.type = type;
    }

    public ExpenseType getType() {
        return type;
    }

    public void setType(ExpenseType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
