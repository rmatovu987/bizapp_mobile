/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.expenses;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.util.Base64;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class ExpenseList {

    InitForm initForm;
    Image icon;

    public ExpenseList(InitForm initForm) {
        this.initForm = initForm;
        fetchData();
    }

    RichComponents rich = new RichComponents();
    ArrayList<Expense> ShowData = new ArrayList<>();
    ArrayList<Expense> OriginalData = new ArrayList<>();
    InfiniteContainer List;

    public void Home() {
        Form mainForm = initForm.mainForm(null, "Expenses", "White-Background", false, false, true);
        FloatingActionButton refreshbutton = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        refreshbutton.addActionListener(e -> {
            new NewExpense(initForm, false, new Expense()).Home();
        });
        refreshbutton.getStyle().setBgColor(ColorUtil.rgb(0, 84, 208));
        refreshbutton.bindFabToContainer(mainForm.getContentPane());
        refreshbutton.setDraggable(true);

        initForm.mainToolbar.addSearchCommand(e -> {
            String text = (String) e.getSource();
            ShowData.clear();
            if (text == null || text.length() == 0) {
                // clear search
                for (Expense i : OriginalData) {
                    ShowData.add(i);
                }
                mainForm.getContentPane().animateLayout(150);
            } else {
                text = text.toLowerCase();
                for (int i = 0; i < OriginalData.size(); i++) {
                    if (OriginalData.get(i).getType().getName().toLowerCase().contains(text.toLowerCase())) {
                        ShowData.add(OriginalData.get(i));
                    }
                }
                mainForm.getContentPane().animateLayout(150);
            }
            List.refresh();
        }, 4);

        mainForm.add(BorderLayout.CENTER, ExpenseList());
        mainForm.show();
    }

    private Container ExpenseList() {
        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (OriginalData.isEmpty()) {
                        fetchData();
                    }
                }
                if (index + amount > ShowData.size()) {
                    amount = ShowData.size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {
                    try {
                        Expense acc = ShowData.get(index + itr);
                        MultiButton mb = new MultiButton();
                        mb.add(BorderLayout.CENTER, ExpenseElement(acc));
                        mb.addActionListener(e -> {
                            new NewExpense(initForm, true, acc).Home();
                        });
                        more[itr] = mb;
                    } catch (JSONException ex) {
                    }
                }
                return more;
            }
        };

        return List;
    }

    private void fetchData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "expenses")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                OriginalData.clear();
                ShowData.clear();

                JSONArray data = new JSONArray(result.getResponseData());

                if (data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject item = data.getJSONObject(i);
                        try {
                            String url = item.getJSONObject("attachment").getString("url");
                            byte[] b = Base64.decode(url.getBytes());
                            icon = EncodedImage.create(b);
                        } catch (JSONException e) {
                        }

                        JSONObject dd = item.getJSONObject("type");

                        Expense itemx = new Expense(
                                item.getLong("id"),
                                item.getString("date"),
                                item.getString("description"),
                                item.getDouble("cost"),
                                new ExpenseType(dd.getLong("id"), dd.getString("name")),
                                icon
                        );
                        OriginalData.add(itemx);
                        ShowData.add(itemx);
                    }
                }
            }
        } catch (JSONException ex) {
        }
    }

    private Container ExpenseElement(Expense income) throws JSONException {
        Image photoIcon = income.getPhoto();

        L10NManager lnm = L10NManager.getInstance();

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillRoundRect(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 10, 10);
        Object mask = roundMask.createMask();
        photoIcon = photoIcon.applyMaskAutoScale(mask);

        Button photo = new Button(photoIcon);
        Style closeStyle = photo.getAllStyles();
        closeStyle.setFgColor(0xffffff);
        closeStyle.setBgTransparency(0);
        closeStyle.setPaddingUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setPadding(1, 1, 1, 1);
        closeStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setMargin(0, 0, 0, 1);

        Label type = new Label(income.getType().getName());
        type.setUIID("List-Line11");
        type.getAllStyles().setAlignment(Component.LEFT);

        Label date = new Label(income.getDate());
        date.setUIID("List-Line22");
        date.getAllStyles().setAlignment(Component.LEFT);

        Label quantity = new Label("Cost: ");
        quantity.setUIID("List-Line33");
        quantity.getAllStyles().setAlignment(Component.RIGHT);

        Label ty = new Label(initForm.business.getJSONObject("currency").getString("code") + " " + lnm.format(income.getCost()));
        ty.setUIID("Red-Pill");
        ty.getAllStyles().setAlignment(Component.CENTER);

        Container photoContainer = BoxLayout.encloseYCenter(new Label(photoIcon));
        photoContainer.getAllStyles().setMarginUnit(Style.UNIT_TYPE_DIPS);
        photoContainer.getAllStyles().setMarginRight(3);

        Container lines = BorderLayout.west(photoContainer);
        lines.add(BorderLayout.CENTER, BoxLayout.encloseYCenter(type, date, BoxLayout.encloseX(quantity, ty)));

        return rich.UnPaddedCard(lines);
    }
}
