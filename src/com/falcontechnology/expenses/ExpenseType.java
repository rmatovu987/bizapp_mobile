/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.expenses;

/**
 *
 * @author falcon
 */
public class ExpenseType {
    private Long id;
    private String name;

    public ExpenseType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ExpenseType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
