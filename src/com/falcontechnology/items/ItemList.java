/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.items;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.home.Dashboard;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class ItemList {

    InitForm initForm;
    InfiniteContainer List;
    Image icon;

    public ItemList(InitForm initForm) {
        this.initForm = initForm;
        fetchData();
    }

    RichComponents rich = new RichComponents();
    ArrayList<Item> ShowData = new ArrayList<>();
    ArrayList<Item> OriginalData = new ArrayList<>();

    public void Home() {

        Form mainForm = initForm.mainForm(null, "Items", "White-Background", false, false, true);

        FloatingActionButton refreshbutton = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        refreshbutton.addActionListener(e -> {
            new NewItem(initForm, false, new Item()).Home();
        });
        refreshbutton.getStyle().setBgColor(ColorUtil.rgb(0, 84, 208));
        refreshbutton.bindFabToContainer(mainForm.getContentPane());
        refreshbutton.setDraggable(true);

        initForm.mainToolbar.addSearchCommand(e -> {
            String text = (String) e.getSource();
            ShowData.clear();
            if (text == null || text.length() == 0) {
                // clear search
                for (Item i : OriginalData) {
                    ShowData.add(i);
                }
                mainForm.getContentPane().animateLayout(150);
            } else {
                text = text.toLowerCase();
                for (int i = 0; i < OriginalData.size(); i++) {
                    if (OriginalData.get(i).getName().toLowerCase().contains(text.toLowerCase())) {
                        ShowData.add(OriginalData.get(i));
                    }
                }
                mainForm.getContentPane().animateLayout(150);
            }
            List.refresh();
        }, 4);

        mainForm.add(BorderLayout.CENTER, ItemList());
        mainForm.show();
    }

    public Container ItemList() {

        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (OriginalData.isEmpty()) {
//                        fetchData();
                    }
                }
                if (index + amount > ShowData.size()) {
                    amount = ShowData.size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {

                    boolean iseven = false;
                    iseven = itr % 2 == 0;
                    Item acc = ShowData.get(index + itr);
                    MultiButton mb = new MultiButton();
                    mb.add(BorderLayout.CENTER, ItemElement(acc, iseven));

                    mb.addActionListener(e -> {
                        new NewItem(initForm, true, acc).Home();
                    });
                    more[itr] = mb;
                }
                return more;
            }
        };

        return List;
    }

    private void fetchData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "items")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .onErrorCodeJSON(errorData -> {
                        if (errorData.getResponseCode() == 402) {
                            rich.SubscriptionDialog();
                        }
                    })
                    .getAsString();

            if (result.getResponseCode() == 200) {
                OriginalData.clear();
                ShowData.clear();
                JSONArray data = new JSONArray(result.getResponseData());
                for (int i = 0; i < data.length(); i++) {
                    JSONObject item = data.getJSONObject(i);

                    try {
                        JSONObject url = item.getJSONObject("photo");
                        Image defaultImage = initForm.theme.getImage("item.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
                        EncodedImage placeholder = (EncodedImage) defaultImage;
                        icon = URLImage.createToStorage(placeholder, "Item" + item.getString("id"), url.getString("url"));

                    } catch (JSONException e) {
//                        icon = initForm.theme.getImage("avater.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
                    }

                    Item itemx = new Item(
                            item.getString("id"),
                            item.getString("name"),
                            item.getString("description"),
                            item.getInt("quantityInStock"),
                            item.getBoolean("trackQuantity"),
                            icon
                    );
                    OriginalData.add(itemx);
                    ShowData.add(itemx);
                }
            }
        } catch (JSONException ex) {
        }
    }

    public Container ItemElement(Item jdata, boolean iseven) {
        Image photoIcon = jdata.getPhoto();

        L10NManager lnm = L10NManager.getInstance();

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillRoundRect(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 10, 10);
        Object mask = roundMask.createMask();
        photoIcon = photoIcon.applyMaskAutoScale(mask);

        Button photo = new Button(photoIcon);
        Style closeStyle = photo.getAllStyles();
        closeStyle.setFgColor(0xffffff);
        closeStyle.setBgTransparency(0);
        closeStyle.setPaddingUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setPadding(1, 1, 1, 1);
        closeStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setMargin(0, 0, 0, 3);

        Label name = new Label(jdata.getName());
        name.setUIID("List-Line1");
        name.getAllStyles().setAlignment(Component.LEFT);

        TextArea description = new TextArea(jdata.getDescription());
        description.setUIID("List-Line2");
        description.getAllStyles().setAlignment(Component.LEFT);

        Label quantity = new Label("Stock");
        quantity.setUIID("List-Line3");
        quantity.getAllStyles().setAlignment(Component.LEFT);

        Label ty = new Label(lnm.format(jdata.getQuantityInStock()));
        ty.setUIID("Light-Blue-Pill");

        Container photoContainer = BoxLayout.encloseYCenter(new Label(photoIcon));
        photoContainer.getAllStyles().setMarginUnit(Style.UNIT_TYPE_DIPS);
        photoContainer.getAllStyles().setMarginRight(3);

        Container last = new Container(new BoxLayout(BoxLayout.X_AXIS));
        if (jdata.isTrackQuantity()) {
            if (jdata.getQuantityInStock() > 10) {
                last.addAll(quantity, ty);
            } else {
                Button error = new Button();
                error.setIcon(materialIcon(FontImage.MATERIAL_ERROR, 2, 0xFF0000));
                error.getAllStyles().setMarginUnit(Style.UNIT_TYPE_DIPS);
                error.getAllStyles().setMargin(0, 0, 0, 0);
                error.getAllStyles().setPaddingUnit(Style.UNIT_TYPE_DIPS);
                error.getAllStyles().setPadding(0, 0, 0, 0);

                last.addAll(error, quantity, ty);
            }
        } else {
            last.addAll(quantity, new Label("Untracked"));
        }

        Container lines = BorderLayout.west(photoContainer);
        lines.add(BorderLayout.CENTER, BoxLayout.encloseYCenter(name, description, last));

        return rich.UnPaddedCard(lines);
    }

}
