/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.items;

import com.codename1.ui.Image;

/**
 *
 * @author falcon
 */
public class Item {
    private String id;
    private String name;
    private String description;
    private double quantityInStock;
    private Image photo;
    private boolean trackQuantity;
    private String creator;

    public Item(String id, String name, String description, double quantityInStock, boolean trackQuantity) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.quantityInStock = quantityInStock;
        this.trackQuantity = trackQuantity;
    }

    public Item() {
    }

    public Item(String id, String name, String description, double quantityInStock, boolean trackQuantity, Image photo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.quantityInStock = quantityInStock;
        this.trackQuantity = trackQuantity;
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(double quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public boolean isTrackQuantity() {
        return trackQuantity;
    }

    public void setTrackQuantity(boolean trackQuantity) {
        this.trackQuantity = trackQuantity;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
