/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.reports;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.items.Item;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class ItemReport {

    InitForm initForm;

    public ItemReport(InitForm initForm) {
        this.initForm = initForm;
        fetchData();
    }

    RichComponents rich = new RichComponents();
    ArrayList<Item> OriginalData = new ArrayList<>();
    int totItems = 0;

    public void Home() {
        Form mainForm = initForm.subForm(null, "Item report", "White-Background", false, true);
        mainForm.getToolbar().setBackCommand("", e -> new ReportList(initForm).Home());

        Label name = new Label("Name");
        name.getAllStyles().setAlignment(Component.LEFT);

        Label qt = new Label("Quantity in stock");
        qt.getAllStyles().setAlignment(Component.CENTER);

        Label cr = new Label("Creator");
        cr.getAllStyles().setAlignment(Component.CENTER);

        Container bb = GridLayout.encloseIn(3, name, qt, cr);

        Container m = BoxLayout.encloseY(bb, new Label("Description"));
        m.setScrollableY(false);
        m.setUIID("Bottoms");

        L10NManager lnm = L10NManager.getInstance();

        Container n = BoxLayout.encloseY(GridLayout.encloseIn(2, new Label("No. of items: "), new Label(lnm.format(totItems))));
        n.setUIID("Totals");

        Container j = BoxLayout.encloseY(ItemList(), n);
        j.setScrollableY(true);

        mainForm.add(BorderLayout.NORTH, m);
        mainForm.add(BorderLayout.CENTER, j);
        mainForm.show();
    }

    private void fetchData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "items")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .onErrorCodeJSON(errorData -> {
                        if (errorData.getResponseCode() == 402) {
                            rich.SubscriptionDialog();
                        }
                    })
                    .getAsString();

            if (result.getResponseCode() == 200) {
                OriginalData.clear();
                JSONArray data = new JSONArray(result.getResponseData());
                for (int i = 0; i < data.length(); i++) {
                    JSONObject item = data.getJSONObject(i);

                    Item itemx = new Item(
                            item.getString("id"),
                            item.getString("name"),
                            item.getString("description"),
                            item.getInt("quantityInStock"),
                            item.getBoolean("trackQuantity")
                    );
                    itemx.setCreator(item.getJSONObject("createdBy").getString("firstName"));
                    totItems = totItems + 1;
                    OriginalData.add(itemx);
                }
            }
        } catch (JSONException ex) {
        }
    }

    InfiniteContainer List;

    public Container ItemList() {

        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (OriginalData.isEmpty()) {
                        fetchData();
                    }
                }
                if (index + amount > OriginalData.size()) {
                    amount = OriginalData.size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {

                    boolean iseven = false;
                    iseven = itr % 2 == 0;
                    Item acc = OriginalData.get(index + itr);

                    more[itr] = ItemElement(acc, iseven);
                }
                return more;
            }
        };

        return List;
    }

    public Container ItemElement(Item jdata, boolean iseven) {
        L10NManager lnm = L10NManager.getInstance();

        Label name = new Label(jdata.getName());
        name.getAllStyles().setAlignment(Component.LEFT);

        TextArea description = new TextArea(jdata.getDescription());
        description.setEditable(false);
        description.setFocusable(false);
        description.setUIID("Label");
        description.getAllStyles().setAlignment(Component.LEFT);

        Label ty = new Label(lnm.format(jdata.getQuantityInStock()));
        ty.getAllStyles().setAlignment(Component.CENTER);

        Label ii = new Label("Untracked");
        ii.getAllStyles().setAlignment(Component.CENTER);
        
        Label ser = new Label(jdata.getCreator());
        ser.getAllStyles().setAlignment(Component.CENTER);

        Container bb = new Container(BoxLayout.y());
        if(jdata.isTrackQuantity()){
            bb.add(GridLayout.encloseIn(3, name, ty, ser));
        }else{
            bb.add(GridLayout.encloseIn(3, name, ii, ser));
        }

        Container u = BoxLayout.encloseY(bb, description);
        u.setUIID("ListElements");

        return u;
    }

}
