/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.reports;

import ca.weblite.codename1.json.JSONException;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.falcontechnology.home.InitForm;

/**
 *
 * @author falcon
 */
public class ReportList {

    InitForm initForm;

    public ReportList(InitForm initForm) {
        this.initForm = initForm;
    }

    public void Home() {
        Form mainForm = initForm.mainForm(null, "Reports", "White-Background", false, false, true);

        Container item = BorderLayout.center(new Label("Item report"));
        item.setUIID("Card");
        Button it = new Button();
        it.addActionListener(e -> {
            new ItemReport(initForm).Home();
        });
        item.setLeadComponent(it);

        Container sales = BorderLayout.center(new Label("Sales report"));
        sales.setUIID("Card");
        Button iqt = new Button();
        iqt.addActionListener(e -> {
            try {
                new SaleReport(initForm).Home();
            } catch (JSONException ex) {
            }
        });
        sales.setLeadComponent(iqt);

        Container purchases = BorderLayout.center(new Label("Purchases report"));
        purchases.setUIID("Card");
        Button iqet = new Button();
        iqet.addActionListener(e -> {
            try {
                new PurchaseReport(initForm).Home();
            } catch (JSONException ex) {
            }
        });
        purchases.setLeadComponent(iqet);

        Container expense = BorderLayout.center(new Label("Expenses report"));
        expense.setUIID("Card");
        Button iet = new Button();
        iet.addActionListener(e -> {
            try {
                new ExpenseReport(initForm).Home();
            } catch (JSONException ex) {
            }
        });
        expense.setLeadComponent(iet);

        Container income = BorderLayout.center(new Label("Incomes report"));
        income.setUIID("Card");
        Button itt = new Button();
        itt.addActionListener(e -> {
            try {
                new IncomeReport(initForm).Home();
            } catch (JSONException ex) {
            }
        });
        income.setLeadComponent(itt);

        Container netIncome = BorderLayout.center(new Label("Net Income report"));
        netIncome.setUIID("Card");
        Button iete = new Button();
        iete.addActionListener(e -> {
            try {
                new NetIncomeReport(initForm).Home();
            } catch (JSONException ex) {
            }
        });
        netIncome.setLeadComponent(iete);

        mainForm.add(BorderLayout.CENTER, BoxLayout.encloseY(item, sales, purchases, expense, income, netIncome));
        mainForm.show();
    }

}
