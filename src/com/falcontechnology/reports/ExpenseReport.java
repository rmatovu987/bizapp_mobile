/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.reports;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.util.ColorUtil;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.expenses.Expense;
import com.falcontechnology.expenses.ExpenseType;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class ExpenseReport {

    InitForm initForm;

    public ExpenseReport(InitForm initForm) {
        this.initForm = initForm;

        fetchIncomeTypeData();
        types = new String[incomeList.size()];
        for (int i = 0; i < incomeList.size(); i++) {
            ExpenseType get = incomeList.get(i);
            types[i] = get.getName();
        }
    }

    RichComponents rich = new RichComponents();
    ArrayList<Expense> OriginalData = new ArrayList<>();
    ArrayList<ExpenseType> incomeList = new ArrayList<>();
    String[] types;
    double totalIncome = 0.0;
    int totIncome = 0;

    public void Home() throws JSONException {
        Form mainForm = initForm.subForm(null, "Expense report", "White-Background", false, true);
        mainForm.getToolbar().add(BorderLayout.EAST, filter());
        mainForm.getToolbar().setBackCommand("", e -> new ReportList(initForm).Home());

        Label name = new Label("Date");
        name.getAllStyles().setAlignment(Component.LEFT);

        Label qt = new Label("Expense");
        qt.getAllStyles().setAlignment(Component.CENTER);

        Label cr = new Label("Creator");
        cr.getAllStyles().setAlignment(Component.CENTER);
        
        Label e = new Label("Expense type");
        e.getAllStyles().setAlignment(Component.CENTER);

        Container bb = GridLayout.encloseIn(4, name, qt, cr, e);

        Container mq = BorderLayout.center(new Label("Description"));
        
        Container l = BoxLayout.encloseY(bb, mq);
        l.setScrollableY(false);
        l.setUIID("Bottoms");

        L10NManager lnm = L10NManager.getInstance();

        Container n = BoxLayout.encloseY(GridLayout.encloseIn(2, new Label("Total expenses: "), new Label(initForm.business.getJSONObject("currency").getString("code") + " " + lnm.format(totalIncome))), GridLayout.encloseIn(2, new Label("No. of expenses: "), new Label(String.valueOf(totIncome))));
        n.setUIID("Totals");
        
        Container j = BoxLayout.encloseY(ItemList(), n);
        j.setScrollableY(true);
        
        if (OriginalData.isEmpty()) {
            mainForm.add(BorderLayout.CENTER, BoxLayout.encloseYCenter(rich.EmptyGeneralPlaceHolder(initForm, "Select period from filter!")));
        } else {
            mainForm.add(BorderLayout.NORTH, l);
            mainForm.add(BorderLayout.CENTER, j);
        }
        mainForm.show();
    }

    private Button filter() {

        Button profiles = new Button("");
        Style closeStyle = profiles.getAllStyles();
        closeStyle.setPaddingUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setPadding(1, 0, 0, 1);
        profiles.setIcon(materialIcon(FontImage.MATERIAL_FILTER_LIST, 3, ColorUtil.WHITE));
        profiles.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {

                FormFiled from = new FormFiled(false, "From*", true, Display.PICKER_TYPE_DATE);
                FormFiled to = new FormFiled(false, "To*", true, Display.PICKER_TYPE_DATE);
                FormFiled type = new FormFiled(false, "Expense type", true, Display.PICKER_TYPE_STRINGS);

                Container pick = new Container(BoxLayout.y());
                
                pick.addAll(from.getFormField(),
                        to.getFormField(),
                        type.getFormField());                

                Dialog dlg = rich.Dialog("Filters");

                type.setPickerData(types);

                Button yes = new Button("Cancel");
                yes.addActionListener(ev -> {
                    dlg.dispose();
                });

                Button save = new Button("Load");
                save.addActionListener(ev -> {
                    fetchData(type.getContent(), from.convertDate(), to.convertDate());
                    dlg.dispose();
                    try {
                        Home();
                    } catch (JSONException ex) {
                    }
                });

                dlg.add(BoxLayout.encloseY(pick));
                dlg.add(GridLayout.encloseIn(2, yes, save));
                dlg.showPacked(BorderLayout.CENTER, true);
            }
        });
        return profiles;
    }

    private void fetchIncomeTypeData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "expenses/types")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                incomeList.clear();

                JSONArray data = new JSONArray(result.getResponseData());
                for (int i = 0; i < data.length(); i++) {
                    JSONObject item = data.getJSONObject(i);

                    ExpenseType itemx = new ExpenseType(
                            item.getLong("id"),
                            item.getString("name")
                    );
                    incomeList.add(itemx);
                }
            }
        } catch (JSONException ex) {
        }
    }

    private void fetchData(String type, String from, String to) {
        String id = "ALL";
        ExpenseType itm = null;
        try {
            for (ExpenseType o : incomeList) {
                if (type.equals(o.getName())) {
                    itm = o;
                }
            }
        } catch (Exception e) {
        }
        try {
            try {
                id = itm.getId().toString();
            } catch (Exception e) {
            }

            Response<String> result = Rest.get(URLLinks.getMainBackend() + "reports/expenses")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .queryParam("start", from)
                    .queryParam("end", to)
                    .queryParam("typeId", id)
                    .onErrorCodeJSON((errorData) -> {
                        System.out.println("err " + errorData.getResponseErrorMessage());
                    })
                    .getAsString();
            if (result.getResponseCode() == 200) {
                OriginalData.clear();

                JSONArray data = new JSONArray(result.getResponseData());

                if (data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject item = data.getJSONObject(i);

                        JSONObject dd = item.getJSONObject("type");

                        Expense itemx = new Expense(
                                item.getLong("id"),
                                item.getString("date"),
                                item.getString("description"),
                                item.getDouble("cost"),
                                new ExpenseType(dd.getLong("id"), dd.getString("name"))
                        );
                        itemx.setCreator(item.getJSONObject("createdBy").getString("firstName"));
                        totalIncome = totalIncome + itemx.getCost();
                        totIncome = totIncome + 1;
                        OriginalData.add(itemx);
                    }
                }
            }
        } catch (JSONException ex) {
        }
    }

    InfiniteContainer List;

    public Container ItemList() {

        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (OriginalData.isEmpty()) {
//                        fetchData();
                    }
                }
                if (index + amount > OriginalData.size()) {
                    amount = OriginalData.size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {

                    boolean iseven = false;
                    iseven = itr % 2 == 0;
                    Expense acc = OriginalData.get(index + itr);
                    more[itr] = ItemElement(acc, iseven);
                }
                return more;
            }
        };

        List.setScrollableY(false);
        return List;
    }

    public Container ItemElement(Expense jdata, boolean iseven) {
        L10NManager lnm = L10NManager.getInstance();

        Label name = new Label(jdata.getDate());
        name.getAllStyles().setAlignment(Component.LEFT);

        TextArea description = new TextArea(jdata.getDescription());
        description.setEditable(false);
        description.setFocusable(false);
        description.setUIID("Label");
        description.getAllStyles().setAlignment(Component.LEFT);

        Label ty = new Label(lnm.format(jdata.getCost()));
        ty.getAllStyles().setAlignment(Component.CENTER);

        Label ser = new Label(jdata.getCreator());
        ser.getAllStyles().setAlignment(Component.CENTER);

        Label r = new Label(jdata.getType().getName());
        r.getAllStyles().setAlignment(Component.CENTER);

        Container mq = BorderLayout.center(description);

        Container bb = GridLayout.encloseIn(4, name, ty, ser,r);
        
        Container u = BoxLayout.encloseY(bb, mq);
        u.setUIID("ListElements");
        
        return u;
    }
}
