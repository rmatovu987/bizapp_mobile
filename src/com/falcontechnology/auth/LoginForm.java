/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.auth;

import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.SpanButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import com.codename1.components.ToastBar.Status;
import com.codename1.io.Preferences;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Dialog;
import com.codename1.ui.Font;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.signup.SignUpForm;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;

/**
 *
 * @author falcon
 */
public class LoginForm extends Form {

    RichComponents rich = new RichComponents();

    public LoginForm(Resources theme) {

        super(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER));

        setUIID("LoginForm-Background");

//        getTitleArea().setUIID("Container");
        Image profilePic = theme.getImage("blueicon.png").scaled(rich.minScreensize() / 5, rich.minScreensize() / 5);
        Label profilePicLabel = new Label(profilePic, "Logo-Area");

        Container mm = BoxLayout.encloseY(FlowLayout.encloseCenter(profilePicLabel), LoginFields(theme)
        );
        mm.setScrollableY(true);

        add(BorderLayout.CENTER, mm).add(BorderLayout.SOUTH, CopyRight());

    }

    boolean isSigningup = false;
    boolean isLoggingIn = false;
    boolean isForgot = false;

    private Container LoginFields(Resources theme) {

        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");

        TextField login = new TextField("", "Email address", 25, TextField.EMAILADDR);
        login.setText(Preferences.get("lastLoginEmail", ""));
        login.setUIID("Login-TextFields");
        login.getAllStyles().setMargin(LEFT, 0);
        Label loginIcon = new Label("", "Login-TextFields");
        loginIcon.getAllStyles().setMargin(RIGHT, 0);

        Image tr = FontImage.createFixed("\ue803", fnt, 0xffffff, Display.getInstance().convertToPixels(4), Display.getInstance().convertToPixels(4));

        Container loginbox = BorderLayout.centerEastWest(login, null, new Label(tr));
        loginbox.setUIID("Login-Field-Container");

        TextField password = new TextField("", "Password", 20, TextField.PASSWORD);
        password.setText(Preferences.get("lastLoginPassword", ""));
        password.setUIID("Login-TextFields");
        password.getAllStyles().setMargin(LEFT, 0);
        Label passwordIcon = new Label("", "Login-TextFields");
        passwordIcon.getAllStyles().setMargin(RIGHT, 0);

        Button maskAndUnmaskCheck = new Button("", "Mask-Button");
        maskAndUnmaskCheck.setToggle(true);
        maskAndUnmaskCheck.setMaterialIcon(FontImage.MATERIAL_VISIBILITY_OFF);
        maskAndUnmaskCheck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (password.getConstraint() == TextField.PASSWORD) {
                    password.setConstraint(TextField.NON_PREDICTIVE);
                    maskAndUnmaskCheck.setMaterialIcon(FontImage.MATERIAL_VISIBILITY);
                } else {
                    password.setConstraint(TextField.PASSWORD);
                    maskAndUnmaskCheck.setMaterialIcon(FontImage.MATERIAL_VISIBILITY_OFF);
                }
                if (password.isEditing()) {
                    password.stopEditing();
                    password.startEditingAsync();
                } else {
                    password.getParent().revalidate();
                }
            }
        });

        Image yt = FontImage.createFixed("\ue801", fnt, 0xffffff, Display.getInstance().convertToPixels(4), Display.getInstance().convertToPixels(4));
        Container passwordbox = BorderLayout.centerEastWest(password, FlowLayout.encloseRightMiddle(maskAndUnmaskCheck), new Label(yt));
        passwordbox.setUIID("Login-Field-Container");

        CheckBox cb1 = new CheckBox(" Remember me");
        cb1.setOppositeSide(false);
        cb1.getAllStyles().setMarginUnit(Style.UNIT_TYPE_DIPS);
        cb1.getAllStyles().setMargin(1, 1, 0, 0);
        cb1.getAllStyles().setFgColor(ColorUtil.WHITE);
                
        if(Preferences.get("rememberMe", "").equals("true")){
            cb1.setSelected(true);
        }

        /*
         ----------------------------------
         LoginForm Button 
         ----------------------------------
         */
        Button loginButton = rich.blueButton("Login");
        loginButton.addActionListener((ActionEvent e) -> {
            try {
                if (!isLoggingIn) {
                    isLoggingIn = true;
                    JSONObject reqJson = new JSONObject();
                    reqJson.put("username", login.getText());
                    reqJson.put("password", password.getText());

                    Response<String> result = Rest.post(URLLinks.getMainBackend() + "auth/login")
                            .body(reqJson.toString())
                            .jsonContent()
                            .onErrorCodeJSON((errorData) -> {
                                if (errorData.getResponseCode() == 401) {
                                    Dialog dlg = rich.Dialog("Unauthorized");

                                    Button yes = new Button("Close");
                                    yes.addActionListener(ev -> {
                                        dlg.dispose();
                                        isLoggingIn = false;
                                    });

                                    dlg.add(new SpanLabel("Wrong credentials. Try again!"));
                                    dlg.add(FlowLayout.encloseRight(yes));
                                    dlg.showPacked(BorderLayout.CENTER, true);
                                }
                            })
                            .getAsString();

                    if (result.getResponseCode() == 200) {
                        if (cb1.isSelected()) {
                            Preferences.set("rememberMe", "true");
                            Preferences.set("lastLoginEmail", login.getText());
                            Preferences.set("lastLoginPassword", password.getText());
                        }else{
                            Preferences.clearAll();
                        }
                        InitForm form = new InitForm(theme, new JSONObject(result.getResponseData()));
                        form.Home();
                    }
                    isLoggingIn = false;
                }
            } catch (Exception ev) {
            }
        });

        /*
         ----------------------------------
         ForgotPassWord Button link
         ----------------------------------
         */
        Button createNewAccount = new Button("Forgot credentials?");
        createNewAccount.setUIID("Forgot-Button");
        createNewAccount.addActionListener(p -> {
            Dialog pot = rich.Dialog("Reset Password");
            FormFiled email = new FormFiled(false, "Email*");
            email.setIsEmail(true);

            pot.add(email.getFormField());

            Button yest = new Button("Reset");
            yest.addActionListener(ev -> {
                if (!email.getContent().isEmpty() || !email.getContent().equals("")) {
                    try {
                        if (!isForgot) {
                            isForgot = true;
                            JSONObject reqJson = new JSONObject();
                            reqJson.put("email", email.getContent());

                            Response<String> result = Rest.post(URLLinks.getMainBackend() + "auth/resetPassword")
                                    .body(reqJson.toString())
                                    .jsonContent()
                                    .onErrorCodeJSON((errorData) -> {
                                        if (errorData.getResponseCode() == 401) {
                                            pot.dispose();
                                            isForgot = false;
                                            rich.WarningSnackbar("Wrong credentials. Try again!");
                                        }
                                    })
                                    .getAsString();

                            if (result.getResponseCode() == 200) {
                                pot.dispose();

                                Dialog dlg = rich.Dialog("Reset Password");
                                Button yes = new Button("Close");
                                yes.addActionListener(eve -> {
                                    dlg.dispose();
                                    new LoginForm(theme).show();
                                });

                                dlg.add(new SpanLabel("Check your email for the new password!"));
                                dlg.add(FlowLayout.encloseRight(yes));
                                dlg.showPacked(BorderLayout.CENTER, true);

                            }
                            isForgot = false;
                        }
                    } catch (JSONException e) {
                    }
                }
            });
            pot.setDisposeWhenPointerOutOfBounds(true);
            pot.add(FlowLayout.encloseRight(yest));
            pot.showPacked(BorderLayout.CENTER, true);

        });

        /*
         ----------------------------------
         Sign Up Button link
         ----------------------------------
         */
        Button signUp = new Button("Sign Up");
        signUp.addActionListener(e -> {
            if (!isSigningup) {
                isSigningup = true;

                Status status = rich.LoadingSnackbar("Loading...");
                status.setShowProgressIndicator(true);
                status.show();
                new SignUpForm(theme).show();
                status.clear();

                isSigningup = false;
            }

        });
        signUp.setUIID("SignUp-Button");
        Label doneHaveAnAccount = new Label("Don't have an account?", "Dont-Have-Account-Login-Label");

        /*
         ----------------------------------
         LoginForm fields mother container 
         ----------------------------------
         */
        Container mothercnt = BoxLayout.encloseY(
                createLineSeparator(),
                loginbox,
                passwordbox,
                GridLayout.encloseIn(2, cb1, createNewAccount),
                loginButton,
                FlowLayout.encloseCenter(doneHaveAnAccount, signUp),
                createLineSeparator()
        );
        mothercnt.setUIID("LoginFields-Container");

        return mothercnt;
    }

    private Container CopyRight() {

        SpanButton right = new SpanButton("Falcon Technologies", "Copyright");
        right.setIcon(materialIcon(FontImage.MATERIAL_COPYRIGHT, 2, ColorUtil.WHITE));
//        right.addActionListener((e) -> Display.getInstance().execute("https://arkounts.com/#/"));
        Container copyright = BorderLayout.centerAbsolute(right);

        return copyright;
    }

    private Component createLineSeparator() {
        Label separator = new Label("", "Commons-Space");
        separator.setShowEvenIfBlank(true);
        return separator;
    }
}
