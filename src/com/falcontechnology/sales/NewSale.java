/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.sales;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.capture.Capture;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.io.Log;
import com.codename1.io.Storage;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.getCurrentForm;
import static com.codename1.ui.CN.isPortrait;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.GeneralPath;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.ImageIO;
import com.codename1.util.Base64;
import com.codename1.util.OnComplete;
import com.codename1.util.StringUtil;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.items.Item;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class NewSale {

    InitForm initForm;
    Sale sale;
    boolean editing;
    Image icon;
    Label logoimage;
    String userImage64 = "";
    ArrayList<Item> itemList = new ArrayList<>();
    String[] itemData;

    public NewSale(InitForm initForm, boolean editing, Sale sale) {
        this.initForm = initForm;
        this.editing = editing;
        this.sale = sale;

        if (sale.getItems() == null) {
            ArrayList<SaleItem> entries = new ArrayList<>();
            sale.setItems(entries);
        }

        fetchItemData();
        itemData = new String[itemList.size()];
        for (int i = 0; i < itemList.size(); i++) {
            Item get = itemList.get(i);
            itemData[i] = get.getName();
        }
    }

    RichComponents rich = new RichComponents();

    public void Home() {
        Form mainForm = initForm.subForm("Sale details", "New sale", "WhiteSmoke-Background", editing, true);
        mainForm.getToolbar().setBackCommand("", er -> {
            new SalesList(initForm).Home();
        });

        if (editing) {
            initForm.mainToolbar.addCommandToRightBar("", materialIcon(FontImage.MATERIAL_DELETE_SWEEP, 4, ColorUtil.WHITE), e -> {
                try {
                    Response<String> result = Rest.delete(URLLinks.getMainBackend() + "sales/" + sale.getId())
                            .jsonContent()
                            .bearer(initForm.data.getString("jwt"))
                            .header("token", initForm.data.getString("token"))
                            .pathParam("id", sale.getId())
                            .onErrorCodeJSON((errorData) -> {
                                if (errorData.getResponseCode() == 404) {
                                    Dialog dlg = rich.Dialog("Not found!");

                                    Button yes = new Button("Close");
                                    yes.addActionListener(ev -> {
                                        dlg.dispose();
                                    });

                                    dlg.add(new SpanLabel("Sale doesn't exist!"));
                                    dlg.add(FlowLayout.encloseRight(yes));
                                    dlg.showPacked(BorderLayout.CENTER, true);
                                } else if (errorData.getResponseCode() == 402) {
                                    rich.SubscriptionDialog();
                                }
                            })
                            .getAsString();

                    if (result.getResponseCode() == 200) {
                        rich.SuccessSnackbar("Sale deleted!");

                        new SalesList(initForm).Home();
                    }
                } catch (Exception et) {
                }
            });
        } else {
            initForm.mainToolbar.addCommandToRightBar("", materialIcon(FontImage.MATERIAL_DONE, 4, ColorUtil.WHITE), e -> {
                if (date.getDateContent() != null && !receipt.getContent().equals("")) {
                    if (sale.getItems().size() > 0) {
                        sale.setDate(date.convertDate());
                        sale.setReceiptNumber(receipt.getContent());
                        saveSale();
                    } else {
                        Dialog dlg = rich.Dialog("No sale items");
                        Button yes = new Button("Close");
                        yes.addActionListener(ev -> {
                            dlg.dispose();
                        });

                        dlg.add(new SpanLabel("A sale without any sale items cannot be saved!"));
                        dlg.add(FlowLayout.encloseRight(yes));
                        dlg.showPacked(BorderLayout.CENTER, true);
                    }
                } else {
                    rich.MandatoryDialog();
                }
            });
        }

        Container tt = BoxLayout.encloseY(top());
        tt.add(ItemList());

        mainForm.add(BorderLayout.CENTER, tt);
        mainForm.show();
    }

    FormFiled date = new FormFiled(true, "Date*", true, Display.PICKER_TYPE_DATE);
    FormFiled receipt = new FormFiled(true, "Receipt number*");

    private Container top() {
        try {
            date.setPickerDate(sale.getDate());
            receipt.setContentBeforeShow(sale.getReceiptNumber());
        } catch (Exception e) {
        }
        Button newholder = new Button("Add sale item");
        newholder.setUIID("FCI-NewClient-Top-Link");
        newholder.addActionListener(e -> {
            saleItemEntry(new SaleItem(), false);
        });

        Button updateDetails = new Button("Update");
        updateDetails.setUIID("FCI-NewClient-Top-Link");
        updateDetails.addActionListener(e -> {
            sale.setDate(date.convertDate());
            sale.setReceiptNumber(receipt.getContent());
            updateDetails();
        });

        Container top = new Container(new BorderLayout());
        top.setUIID("FCI-NewClient-TopBackground");

        top.add(BorderLayout.WEST, Photo());
        top.add(BorderLayout.CENTER, BoxLayout.encloseY(date.getFormField(), receipt.getFormField()));
        if (editing) {
            top.add(BorderLayout.SOUTH, GridLayout.encloseIn(2, newholder, updateDetails));
        } else {
            top.add(BorderLayout.SOUTH, newholder);
        }

        return top;
    }

    private void updateDetails() {
        try {
            JSONObject reqJson = new JSONObject();
            reqJson.put("receiptNumber", sale.getReceiptNumber());
            reqJson.put("date", sale.getDate());

            Response<String> result = Rest.put(URLLinks.getMainBackend() + "sales/" + sale.getId())
                    .body(reqJson.toString())
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .pathParam("id", sale.getId())
                    .onErrorCodeJSON((errorData) -> {
                        if (errorData.getResponseCode() == 404) {
                            Dialog dlg = rich.Dialog("Not found!");

                            Button yes = new Button("Close");
                            yes.addActionListener(ev -> {
                                dlg.dispose();
                            });

                            dlg.add(new SpanLabel("Sale doesn't exist!"));
                            dlg.add(FlowLayout.encloseRight(yes));
                            dlg.showPacked(BorderLayout.CENTER, true);
                        } else if (errorData.getResponseCode() == 402) {
                            rich.SubscriptionDialog();
                        }
                    })
                    .getAsString();

            if (result.getResponseCode() == 200) {
                rich.SuccessSnackbar("Date and receipt number updated!");

                new SalesList(initForm).Home();
            }
        } catch (Exception e) {
        }
    }

    private Container Photo() {

        Container logoimageCnt = BoxLayout.encloseX(FlowLayout.encloseCenter(ReceiptPhoto()));
        Style logoStyle = logoimageCnt.getAllStyles();
        logoStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        logoStyle.setMargin(0, 0, 0, 0);

        Container fot = new Container(new LayeredLayout());
        fot.add(logoimageCnt);
        fot.add(FlowLayout.encloseRightBottom(updatePhotoBtn()));

        Container photobox = BoxLayout.encloseY(fot);

        Container avatarBox = BorderLayout.centerAbsolute(photobox);
        avatarBox.setUIID("Top-Container");

        return avatarBox;
    }

    private Container ReceiptPhoto() {

        if (sale.getPhoto() == null) {
            icon = initForm.theme.getImage("sale.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
        } else {
            icon = sale.getPhoto();
        }

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);
        Object mask = roundMask.createMask();
        Image iconic = icon.applyMask(mask);
        logoimage = new Label("", "Avater-Border");
        logoimage.setIcon(iconic);

        Container logoimageCnt = BoxLayout.encloseX(FlowLayout.encloseCenter(logoimage));

        Button i = new Button();
        i.addActionListener(er -> {
            Form io = initForm.ImageViewer(icon);
            io.show();
        });
        logoimageCnt.setLeadComponent(i);

        return logoimageCnt;
    }

    private Button updatePhotoBtn() {

        Button uploadPhoto = new Button("", "Camera-Icons");
        uploadPhoto.setIcon(materialIcon(FontImage.MATERIAL_CAMERA_ALT, 3, ColorUtil.WHITE));

        uploadPhoto.addActionListener(evt -> {
            String i = Capture.capturePhoto();
            if (i != null) {

                try {
                    final Image newImage = Image.createImage(i);
                    Image roundedMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
                    Graphics gra = roundedMask.getGraphics();
                    gra.setColor(0xffffff);
                    gra.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);

                    Object masked = roundedMask.createMask();

                    cropImage(newImage, rich.minScreensize() / 4, rich.minScreensize() / 4, et -> {

                        if (editing) {
                            try {
                                ImageIO img = ImageIO.getImageIO();
                                ByteArrayOutputStream out = new ByteArrayOutputStream();
                                img.save(et, out, ImageIO.FORMAT_JPEG, 1);
                                byte[] ba = out.toByteArray();
                                userImage64 = Base64.encode(ba);
                                et = et.applyMask(masked);

                                logoimage.setIcon(et);
                                try {
                                    Storage.getInstance().deleteStorageFile("Sale" + sale.getId());

                                    JSONObject reqJson = new JSONObject();
                                    reqJson.put("fileName", "Image" + String.valueOf(System.currentTimeMillis()));
                                    reqJson.put("content", userImage64);

                                    Response<String> result = Rest.put(URLLinks.getMainBackend() + "sales/photo/" + sale.getId())
                                            .body(reqJson.toString())
                                            .jsonContent()
                                            .bearer(initForm.data.getString("jwt"))
                                            .header("token", initForm.data.getString("token"))
                                            .pathParam("id", sale.getId())
                                            .onErrorCodeJSON((errorData) -> {
                                                if (errorData.getResponseCode() == 404) {
                                                    Dialog dlg = rich.Dialog("Not found!");

                                                    Button yes = new Button("Close");
                                                    yes.addActionListener(ev -> {
                                                        dlg.dispose();
                                                    });

                                                    dlg.add(new SpanLabel("Sale doesn't exist!"));
                                                    dlg.add(FlowLayout.encloseRight(yes));
                                                    dlg.showPacked(BorderLayout.CENTER, true);
                                                } else if (errorData.getResponseCode() == 402) {
                                                    rich.SubscriptionDialog();
                                                }
                                            })
                                            .getAsString();

                                    if (result.getResponseCode() == 200) {
                                        rich.SuccessSnackbar("Photo updated!");

                                        Home();
                                    }
                                } catch (Exception e) {
                                }
//                                logoimage.getComponentForm().revalidate();
                            } catch (IOException ex) {

                            }
                        } else {
                            et = et.applyMask(masked);

                            logoimage.setIcon(et);
                        }

                    });

                } catch (IOException ex) {
                    Log.p("Error loading captured image from camera", Log.ERROR);
                }
            }
        });

        return uploadPhoto;
    }

    private void cropImage(Image img, int destWidth, int destHeight, OnComplete<Image> s) {

        Form previous = getCurrentForm();
        Form cropForm = new Form("", new LayeredLayout());

        Label toobarLabel = new Label("New Holder", "Toolbar-HeaderLabel");
        cropForm.setTitleComponent(toobarLabel);

        Toolbar mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        cropForm.setToolbar(mainToolbar);

        Label moveAndZoom = new Label("Move and zoom the photo to crop it");
        moveAndZoom.getUnselectedStyle().setFgColor(0xffffff);
        moveAndZoom.getUnselectedStyle().setAlignment(CENTER);
        moveAndZoom.setCellRenderer(true);
        cropForm.setGlassPane((Graphics g, Rectangle rect) -> {
            g.setColor(0x0000ff);
            g.setAlpha(150);
            Container cropCp = cropForm.getContentPane();
            int posY = cropForm.getContentPane().getAbsoluteY();

            GeneralPath p = new GeneralPath();
            p.setRect(new Rectangle(0, posY, cropCp.getWidth(), cropCp.getHeight()), null);
            if (isPortrait()) {
                p.arc(0, posY + cropCp.getHeight() / 2 - cropCp.getWidth() / 2,
                        cropCp.getWidth() - 1, cropCp.getWidth() - 1, 0, Math.PI * 2);
            } else {
                p.arc(cropCp.getWidth() / 2 - cropCp.getHeight() / 2, posY,
                        cropCp.getHeight() - 1, cropCp.getHeight() - 1, 0, Math.PI * 2);
            }
            g.fillShape(p);
            g.setAlpha(255);
            g.setColor(0xffffff);
            moveAndZoom.setX(0);
            moveAndZoom.setY(posY);
            moveAndZoom.setWidth(cropCp.getWidth());
            moveAndZoom.setHeight(moveAndZoom.getPreferredH());
            moveAndZoom.paint(g);
        });

        final ImageViewer viewer = new ImageViewer();
        viewer.setImage(img);

        cropForm.add(viewer);
        cropForm.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_CROP, e -> {
            previous.showBack();
            s.completed(viewer.getImage().
                    fill(destWidth, destHeight));
        });
        cropForm.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_CANCEL, e -> previous.showBack());
        cropForm.show();

    }

    InfiniteContainer List;

    public Container ItemList() {

        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (sale.getItems().isEmpty()) {
                    }
                }
                if (index + amount > sale.getItems().size()) {
                    amount = sale.getItems().size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {

                    try {
                        boolean iseven = false;
                        iseven = itr % 2 == 0;
                        SaleItem acc = sale.getItems().get(index + itr);
                        MultiButton mb = new MultiButton();
                        mb.add(BorderLayout.CENTER, entryLine(acc, iseven));

                        mb.addActionListener(e -> {
                            saleItemEntry(acc, true);
                        });
                        more[itr] = mb;
                    } catch (JSONException ex) {
                    }
                }
                return more;
            }
        };

        return List;
    }

    public void saleItemEntry(SaleItem saleItem, boolean edit) {
        L10NManager lnm = L10NManager.getInstance();

        String title = "";
        if (edit) {
            title = "Sale item details";
        } else {
            title = "New Sale item";
        }

        FormFiled qty = new FormFiled(false, "Quantity*");
        FormFiled price = new FormFiled(false, "Price*");
        FormFiled item = new FormFiled(false, "Item*", true, Display.PICKER_TYPE_STRINGS);
        item.setPickerData(itemData);

        try {
            item.setSelectedPickerString(saleItem.getItem().getName());
            qty.setContentBeforeShow(lnm.format(saleItem.getQuantity()));
            price.setContentBeforeShow(lnm.format(saleItem.getSalePrice()));
        } catch (Exception e) {
        }
        Dialog dlg = rich.Dialog(title);

        Button save = new Button("Save");
        Button update = new Button("Update");
        Button cancel = new Button("Cancel");
        Button delete = new Button("Delete");

        cancel.addActionListener(ev -> {
            dlg.dispose();
        });
        save.addActionListener(l -> {
            if (!qty.getContent().equals("") && !price.getContent().equals("") && item.getContent() != null) {
                if (editing) {
                    Item itm = null;
                    for (Item o : itemList) {
                        if (item.getContent().equals(o.getName())) {
                            itm = o;
                        }
                    }

                    try {
                        JSONObject req = new JSONObject();
                        req.put("quantity", qty.getContent());
                        req.put("itemId", itm.getId());
                        req.put("saleId", sale.getId());
                        req.put("salePrice", StringUtil.replaceAll(price.getContent(), ",", ""));

                        Response<String> result = Rest.post(URLLinks.getMainBackend() + "saleitems")
                                .body(req.toString())
                                .acceptJson()
                                .jsonContent()
                                .bearer(initForm.data.getString("jwt"))
                                .header("token", initForm.data.getString("token"))
                                .getAsString();

                        if (result.getResponseCode() == 201) {
                            rich.SuccessSnackbar("Saved!");

                            new SalesList(initForm).Home();
                        }
                    } catch (JSONException e) {
                    }

                } else {
                    Item itm = null;
                    for (Item o : itemList) {
                        if (item.getContent().equals(o.getName())) {
                            itm = o;
                        }
                    }

                    SaleItem k = new SaleItem(String.valueOf(System.currentTimeMillis()), lnm.parseDouble(qty.getContent()), itm, lnm.parseDouble(price.getContent()));
                    sale.getItems().add(k);
                    dlg.dispose();
                    List.refresh();
                }
            }
        });
        update.addActionListener(ew -> {
            Item itm = null;
            for (Item o : itemList) {
                if (item.getContent().equals(o.getName())) {
                    itm = o;
                }
            }
            if (editing) {
                try {
                    JSONObject req = new JSONObject();
                    req.put("quantity", qty.getContent());
                    req.put("itemId", itm.getId());
                    req.put("salePrice", StringUtil.replaceAll(price.getContent(), ",", ""));

                    Response<String> result = Rest.put(URLLinks.getMainBackend() + "saleitems/" + saleItem.getId())
                            .body(req.toString())
                            .jsonContent()
                            .bearer(initForm.data.getString("jwt"))
                            .header("token", initForm.data.getString("token"))
                            .pathParam("id", saleItem.getId())
                            .onErrorCodeJSON((errorData) -> {
                                if (errorData.getResponseCode() == 404) {
                                    Dialog e = rich.Dialog("Not found!");

                                    Button yes = new Button("Close");
                                    yes.addActionListener(ev -> {
                                        e.dispose();
                                    });

                                    e.add(new SpanLabel("Selected sale item doesn't exist!"));
                                    e.add(FlowLayout.encloseRight(yes));
                                    e.showPacked(BorderLayout.CENTER, true);
                                } else if (errorData.getResponseCode() == 402) {
                                    rich.SubscriptionDialog();
                                }
                            })
                            .getAsString();

                    if (result.getResponseCode() == 200) {
                        rich.SuccessSnackbar("Updated!");

                        new SalesList(initForm).Home();
                    }
                } catch (Exception e) {
                }
            } else {
                saleItem.setItem(itm);
                saleItem.setQuantity(lnm.parseDouble(StringUtil.replaceAll(qty.getContent(), ",", "")));
                saleItem.setSalePrice(lnm.parseDouble(StringUtil.replaceAll(price.getContent(), ",", "")));
            }
        });
        delete.addActionListener(p -> {
            if (editing) {
                try {
                    Response<String> result = Rest.delete(URLLinks.getMainBackend() + "saleitems/" + saleItem.getId())
                            .jsonContent()
                            .bearer(initForm.data.getString("jwt"))
                            .header("token", initForm.data.getString("token"))
                            .pathParam("id", saleItem.getId())
                            .onErrorCodeJSON((errorData) -> {
                                if (errorData.getResponseCode() == 404) {
                                    Dialog e = rich.Dialog("Not found!");

                                    Button yes = new Button("Close");
                                    yes.addActionListener(ev -> {
                                        e.dispose();
                                    });

                                    e.add(new SpanLabel("Selected item doesn't exist!"));
                                    e.add(FlowLayout.encloseRight(yes));
                                    e.showPacked(BorderLayout.CENTER, true);
                                } else if (errorData.getResponseCode() == 402) {
                                    rich.SubscriptionDialog();
                                }
                            })
                            .getAsString();

                    if (result.getResponseCode() == 200) {
                        rich.SuccessSnackbar("Deleted!");

                        new SalesList(initForm).Home();
                    }
                } catch (JSONException e) {
                }
            } else {
                sale.getItems().remove(saleItem);
            }
        });
        Container g = new Container();
        g.removeAll();
        g.add(BoxLayout.encloseY(
                item.getFormField(),
                qty.getFormField(),
                price.getFormField()));
        dlg.add(g);
        if (edit) {
            dlg.add(GridLayout.encloseIn(2, delete, update));
        } else {
            dlg.add(GridLayout.encloseIn(2, cancel, save));
        }
        dlg.setDisposeWhenPointerOutOfBounds(true);
        dlg.showPacked(BorderLayout.CENTER, true);
    }

    public Container entryLine(SaleItem data, boolean iseven) throws JSONException {

        L10NManager lnm = L10NManager.getInstance();

        Label item = new Label(data.getItem().getName());
        item.setUIID("Commons-EntryLine-Black-Medium");
        item.getAllStyles().setAlignment(Component.LEFT);

        Label qty = new Label("Quantity: " + lnm.format(data.getQuantity()));
        qty.setUIID("Commons-EntryLine-Green-Medium");

        Label price = new Label("Price: " + initForm.business.getJSONObject("currency").getString("code") + " " + lnm.format(data.getSalePrice()));
        price.setUIID("Commons-EntryLine-Gray-Medium");

        Container layerd = new Container(BoxLayout.y());
        layerd.add(item);
        layerd.add(qty);
        layerd.add(price);

        if (iseven) {
            layerd.setUIID("Commons-InfiniteList-EvenRow");
        } else {
            layerd.setUIID("Commons-InfiniteList-OddRow");
        }

        return layerd;
    }

    private void fetchItemData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "items")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                itemList.clear();

                JSONArray data = new JSONArray(result.getResponseData());
                for (int i = 0; i < data.length(); i++) {
                    JSONObject item = data.getJSONObject(i);

                    try {
                        JSONObject url = item.getJSONObject("photo").getJSONObject("url");
                        Image defaultImage = initForm.theme.getImage("avater.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
                        EncodedImage placeholder = (EncodedImage) defaultImage;
                        icon = URLImage.createToStorage(placeholder, "profilePic.png", url.getString("url"));

                    } catch (JSONException e) {
                        icon = initForm.theme.getImage("avater.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
                    }

                    Item itemx = new Item(
                            item.getString("id"),
                            item.getString("name"),
                            item.getString("description"),
                            item.getInt("quantityInStock"),
                            item.getBoolean("trackQuantity"),
                            icon
                    );
                    itemList.add(itemx);
                }
            }
        } catch (JSONException ex) {
        }
    }

    private void saveSale() {
        try {
            JSONArray itm = new JSONArray();
            for (SaleItem i : sale.getItems()) {
                JSONObject d = new JSONObject();
                d.put("quantity", i.getQuantity());
                d.put("itemId", i.getItem().getId());
                d.put("salePrice", StringUtil.replaceAll(String.valueOf(i.getSalePrice()), ",", ""));
                itm.put(d);
            }

            JSONObject reqJson = new JSONObject();
            try {
                ImageIO img = ImageIO.getImageIO();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                img.save(icon, out, ImageIO.FORMAT_JPEG, 1);
                byte[] ba = out.toByteArray();
                String imgg = Base64.encode(ba);

                reqJson.put("fileName", "Image" + String.valueOf(System.currentTimeMillis()));
                reqJson.put("content", imgg);
            } catch (Exception e) {
            }

            JSONObject req = new JSONObject();
            req.put("receiptNumber", sale.getReceiptNumber());
            req.put("saleItems", itm);
            req.put("date", sale.getDate());
            req.put("image", reqJson);

            Response<String> result = Rest.post(URLLinks.getMainBackend() + "sales")
                    .body(req.toString())
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 201) {
                rich.SuccessSnackbar("Saved!");

                new SalesList(initForm).Home();
            }

        } catch (JSONException ex) {
        }
    }
}
