/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.sales;

import com.falcontechnology.items.Item;

/**
 *
 * @author falcon
 */
public class SaleItem {
    private String id;
    private String date;
    private double quantity;
    private Item item;
    private double salePrice;
    private String creator;

    public SaleItem() {
    }

    public SaleItem(String id, double quantity, Item item, double salePrice) {
        this.id = id;
        this.quantity = quantity;
        this.item = item;
        this.salePrice = salePrice;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
