/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.incomes;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.capture.Capture;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.Log;
import com.codename1.io.Storage;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.getCurrentForm;
import static com.codename1.ui.CN.isPortrait;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.geom.GeneralPath;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.ImageIO;
import com.codename1.util.Base64;
import com.codename1.util.OnComplete;
import com.codename1.util.StringUtil;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class NewIncome {
    InitForm initForm;
    boolean editing;
    Income income;
    Label logoimage;
    String userImage64 = "";
    
    ArrayList<IncomeType> incomeList = new ArrayList<>();
    String[] types;
    RichComponents rich = new RichComponents();

    public NewIncome(InitForm initForm, boolean editing, Income income) {
        this.initForm = initForm;
        this.editing = editing;
        this.income = income;
        
        fetchIncomeTypeData();
        types = new String[incomeList.size()];
        for (int i = 0; i < incomeList.size(); i++) {
            IncomeType get = incomeList.get(i);
            types[i] = get.getName();
        }
    }
    
    Image icon;
    
    public void Home() {
        Form mainForm = initForm.subForm("Income details", "New income", "Blue-Form-Background", editing, true);
        mainForm.getToolbar().setBackCommand("", r->{
        new IncomeList(initForm).Home();
        });
        
        if (editing) {
            initForm.mainToolbar.addCommandToRightBar("", materialIcon(FontImage.MATERIAL_DELETE_SWEEP, 4, ColorUtil.WHITE), e -> {
                delete();
            });
        }

        Container Usermother = BoxLayout.encloseY(BorderLayout.centerAbsolute(itemFields()));
        Usermother.setScrollableY(true);
        Usermother.setUIID("Bottom-Container");

        Container Companymother = BorderLayout.center(LayeredLayout.encloseIn(Usermother,
                FlowLayout.encloseCenter(TopContainer())));

        mainForm.add(BorderLayout.CENTER, Companymother);
        mainForm.show();
    }

    private Container TopContainer() {

        Container logoimageCnt = BoxLayout.encloseX(FlowLayout.encloseCenter(ItemPhoto()));
        Style logoStyle = logoimageCnt.getAllStyles();
        logoStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        logoStyle.setMargin(0, 0, 0, 0);

        Container fot = new Container(new LayeredLayout());
        fot.add(logoimageCnt);
        fot.add(FlowLayout.encloseRightBottom(updatePhotoBtn()));

        Container photobox = BoxLayout.encloseY(fot);

        Container avatarBox = BorderLayout.centerAbsolute(photobox);
        avatarBox.setUIID("Top-Container");

        return avatarBox;
    }

    private Container ItemPhoto() {

        if (income.getPhoto() == null) {
            icon = initForm.theme.getImage("income.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
        } else {
            icon = income.getPhoto();
        }

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);
        Object mask = roundMask.createMask();
        Image iconic = icon.applyMask(mask);
        logoimage = new Label("", "Avater-Border");
        logoimage.setIcon(iconic);

        Container logoimageCnt = BoxLayout.encloseX(FlowLayout.encloseCenter(logoimage));
        
        Button i = new Button();
        i.addActionListener(er -> {
            Form io = initForm.ImageViewer(icon);
            io.show();
        });
        logoimageCnt.setLeadComponent(i);

        return logoimageCnt;
    }

    private Button updatePhotoBtn() {

        Button uploadPhoto = new Button("", "Camera-Icons");
        uploadPhoto.setIcon(materialIcon(FontImage.MATERIAL_CAMERA_ALT, 3, ColorUtil.WHITE));

        uploadPhoto.addActionListener(evt -> {
            String i = Capture.capturePhoto();
            if (i != null) {

                try {
                    final Image newImage = Image.createImage(i);
                    Image roundedMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
                    Graphics gra = roundedMask.getGraphics();
                    gra.setColor(0xffffff);
                    gra.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);

                    Object masked = roundedMask.createMask();

                    cropImage(newImage, rich.minScreensize() / 4, rich.minScreensize() / 4, et -> {

                        if (editing) {
                            try {
                                ImageIO img = ImageIO.getImageIO();
                                ByteArrayOutputStream out = new ByteArrayOutputStream();
                                img.save(et, out, ImageIO.FORMAT_JPEG, 1);
                                byte[] ba = out.toByteArray();
                                userImage64 = Base64.encode(ba);
                                et = et.applyMask(masked);

                                logoimage.setIcon(et);
                                try {
                                    Storage.getInstance().deleteStorageFile("Income" + income.getId());

                                    JSONObject reqJson = new JSONObject();
                                    reqJson.put("fileName", "Image" + String.valueOf(System.currentTimeMillis()));
                                    reqJson.put("content", userImage64);

                                    Response<String> result = Rest.put(URLLinks.getMainBackend() + "incomes/photo/" + income.getId())
                                            .body(reqJson.toString())
                                            .jsonContent()
                                            .bearer(initForm.data.getString("jwt"))
                                            .header("token", initForm.data.getString("token"))
                                            .pathParam("id", String.valueOf(income.getId()))
                                            .onErrorCodeJSON((errorData) -> {
                                                if (errorData.getResponseCode() == 404) {
                                                    Dialog dlg = rich.Dialog("Not found!");

                                                    Button yes = new Button("Close");
                                                    yes.addActionListener(ev -> {
                                                        dlg.dispose();
                                                    });

                                                    dlg.add(new SpanLabel("Selected income doesn't exist!"));
                                                    dlg.add(FlowLayout.encloseRight(yes));
                                                    dlg.showPacked(BorderLayout.CENTER, true);
                                                } else if (errorData.getResponseCode() == 402) {
                                                    rich.SubscriptionDialog();
                                                }
                                            })
                                            .getAsString();

                                    if (result.getResponseCode() == 200) {
                                        rich.SuccessSnackbar("Photo updated!");

                                        Home();
                                    }
                                } catch (Exception e) {
                                }
                                logoimage.getComponentForm().revalidate();
                            } catch (IOException ex) {

                            }
                        } else {
                            et = et.applyMask(masked);

                            logoimage.setIcon(et);
                        }

                    });

                } catch (IOException ex) {
                    Log.p("Error loading captured image from camera", Log.ERROR);
                }
            }
        });

        return uploadPhoto;
    }

    private void cropImage(Image img, int destWidth, int destHeight, OnComplete<Image> s) {

        Form previous = getCurrentForm();
        Form cropForm = new Form("", new LayeredLayout());

        Label toobarLabel = new Label("New Holder", "Toolbar-HeaderLabel");
        cropForm.setTitleComponent(toobarLabel);

        Toolbar mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        cropForm.setToolbar(mainToolbar);

        Label moveAndZoom = new Label("Move and zoom the photo to crop it");
        moveAndZoom.getUnselectedStyle().setFgColor(0xffffff);
        moveAndZoom.getUnselectedStyle().setAlignment(CENTER);
        moveAndZoom.setCellRenderer(true);
        cropForm.setGlassPane((Graphics g, Rectangle rect) -> {
            g.setColor(0x0000ff);
            g.setAlpha(150);
            Container cropCp = cropForm.getContentPane();
            int posY = cropForm.getContentPane().getAbsoluteY();

            GeneralPath p = new GeneralPath();
            p.setRect(new Rectangle(0, posY, cropCp.getWidth(), cropCp.getHeight()), null);
            if (isPortrait()) {
                p.arc(0, posY + cropCp.getHeight() / 2 - cropCp.getWidth() / 2,
                        cropCp.getWidth() - 1, cropCp.getWidth() - 1, 0, Math.PI * 2);
            } else {
                p.arc(cropCp.getWidth() / 2 - cropCp.getHeight() / 2, posY,
                        cropCp.getHeight() - 1, cropCp.getHeight() - 1, 0, Math.PI * 2);
            }
            g.fillShape(p);
            g.setAlpha(255);
            g.setColor(0xffffff);
            moveAndZoom.setX(0);
            moveAndZoom.setY(posY);
            moveAndZoom.setWidth(cropCp.getWidth());
            moveAndZoom.setHeight(moveAndZoom.getPreferredH());
            moveAndZoom.paint(g);
        });

        final ImageViewer viewer = new ImageViewer();
        viewer.setImage(img);

        cropForm.add(viewer);
        cropForm.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_CROP, e -> {
            previous.showBack();
            s.completed(viewer.getImage().
                    fill(destWidth, destHeight));
        });
        cropForm.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_CANCEL, e -> previous.showBack());
        cropForm.show();

    }

    private Container itemFields() {
        L10NManager lnm = L10NManager.getInstance();

        Container itemy = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        FormFiled type = new FormFiled(false, "Type*",true, Display.PICKER_TYPE_STRINGS);
        type.setPickerData(types);
        FormFiled description = new FormFiled(false, "Description");
        FormFiled date = new FormFiled(false, "Date*", true, Display.PICKER_TYPE_DATE);
        FormFiled revenue = new FormFiled(false, "Revenue*");

        try {
            type.setSelectedPickerString(income.getType().getName());
            description.setContentBeforeShow(income.getDescription());
            date.setPickerDate(income.getDate());
            revenue.setContentBeforeShow(lnm.format(income.getRevenue()));
        } catch (Exception e) {
        }

        Button save = rich.blueButton("Save");
        save.addActionListener(ev -> {
            if (!type.getContent().equals("") 
                    &&!date.convertDate().equals("") && !revenue.getContent().equals("")) {
                IncomeType itm = null;
                for (IncomeType o : incomeList) {
                    if (type.getContent().equals(o.getName())) {
                        itm = o;
                    }
                }
                income.setDescription(description.getContent());
                income.setRevenue(Double.parseDouble(StringUtil.replaceAll(revenue.getContent(), ",","")));
                income.setDate(date.convertDate());
                income.setType(itm);
                try {
                    backendItem(true, income);
                } catch (IOException ex) {
                }
            } else {
                rich.MandatoryDialog();
            }
        });
        Button update = rich.blueButton("Update");
        update.addActionListener(ev -> {
            IncomeType itm = null;
                for (IncomeType o : incomeList) {
                    if (type.getContent().equals(o.getName())) {
                        itm = o;
                    }
                }
                income.setDescription(description.getContent());
                income.setRevenue(Double.parseDouble(StringUtil.replaceAll(revenue.getContent(), ",","")));
                income.setDate(date.convertDate());
                income.setType(itm);
            try {
                backendItem(false, income);
            } catch (IOException ex) {
            }
        });

        itemy.addAll(date.getFormField(), type.getFormField(), description.getFormField(), revenue.getFormField());

        if (editing) {
            itemy.add(update);
        } else {
            itemy.add(save);
        }

        return itemy;
    }
    
    private void fetchIncomeTypeData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "incomes/types")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                incomeList.clear();

                JSONArray data = new JSONArray(result.getResponseData());
                for (int i = 0; i < data.length(); i++) {
                    JSONObject item = data.getJSONObject(i);

                    IncomeType itemx = new IncomeType(
                            item.getLong("id"),
                            item.getString("name")
                    );
                    incomeList.add(itemx);
                }
            }
        } catch (JSONException ex) {
        }
    }

    private void delete() {
        try {

            Response<String> res = Rest.delete(URLLinks.getMainBackend() + "incomes/" + income.getId())
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .pathParam("id", String.valueOf(income.getId()))
                    .onErrorCodeJSON((errorData) -> {
                switch (errorData.getResponseCode()) {
                    case 404:
                        Dialog dlg = rich.Dialog("Not found!");
                        Button yes = new Button("Close");
                        yes.addActionListener(ev -> {
                            dlg.dispose();
                        });
                        dlg.add(new SpanLabel("Selected income doesn't exist!"));
                        dlg.add(FlowLayout.encloseRight(yes));
                        dlg.showPacked(BorderLayout.CENTER, true);
                        break;
                    case 402:
                        rich.SubscriptionDialog();
                        break;
                    case 403:
                        Dialog dlgd = rich.Dialog("Error!");
                        Button yese = new Button("Close");
                        yese.addActionListener(ev -> {
                            dlgd.dispose();
                        });
                        dlgd.add(new SpanLabel("Income from a sale cannot be deleted!"));
                        dlgd.add(FlowLayout.encloseRight(yese));
                        dlgd.showPacked(BorderLayout.CENTER, true);
                        break;
                    default:
                        break;
                }
                    })
                    .getAsString();

            if (res.getResponseCode() == 200) {
                rich.SuccessSnackbar("Deleted!");

                new IncomeList(initForm).Home();
            }
        } catch (Exception e) {
        }
    }

    private void backendItem(boolean save, Income income) throws IOException {
        if (save) {
            try {
                JSONObject image = new JSONObject();
                if (null != income.getPhoto()) {
                    ImageIO img = ImageIO.getImageIO();
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    img.save(income.getPhoto(), out, ImageIO.FORMAT_JPEG, 1);
                    byte[] ba = out.toByteArray();
                    String images = Base64.encode(ba);

                    image.put("fileName", "Image" + String.valueOf(System.currentTimeMillis()));
                    image.put("content", images);
                }

                JSONObject reqJson = new JSONObject();
                reqJson.put("description", income.getDescription());
                reqJson.put("date", income.getDate());
                reqJson.put("revenue", income.getRevenue());
                reqJson.put("typeId", income.getType().getId());
                reqJson.put("image", image);

                Response<String> result = Rest.post(URLLinks.getMainBackend() + "incomes")
                        .body(reqJson.toString())
                        .jsonContent()
                        .bearer(initForm.data.getString("jwt"))
                        .header("token", initForm.data.getString("token"))
                        .onErrorCodeJSON((errorData) -> {
                            if (errorData.getResponseCode() == 409) {
                                Dialog dlg = rich.Dialog("Conflict");

                                Button yes = new Button("Close");
                                yes.addActionListener(ev -> {
                                    dlg.dispose();
                                });

                                dlg.add(new SpanLabel("Income already exists!"));
                                dlg.add(FlowLayout.encloseRight(yes));
                                dlg.showPacked(BorderLayout.CENTER, true);
                            } else if (errorData.getResponseCode() == 402) {
                                rich.SubscriptionDialog();
                            }
                        })
                        .getAsString();

                if (result.getResponseCode() == 201) {
                    rich.SuccessSnackbar("Saved!");

                    new IncomeList(initForm).Home();
                }
            } catch (JSONException ex) {
            }
        } else {
            try {
                JSONObject reqJson = new JSONObject();
                reqJson.put("description", income.getDescription());
                reqJson.put("date", income.getDate());
                reqJson.put("revenue", income.getRevenue());
                reqJson.put("typeId", income.getType().getId());

                Response<String> res = Rest.put(URLLinks.getMainBackend() + "incomes/" + income.getId())
                        .body(reqJson.toString())
                        .jsonContent()
                        .bearer(initForm.data.getString("jwt"))
                        .header("token", initForm.data.getString("token"))
                        .pathParam("id", String.valueOf(income.getId()))
                        .onErrorCodeJSON((errorData) -> {
                            if (errorData.getResponseCode() == 404) {
                                Dialog dlg = rich.Dialog("Not found!");

                                Button yes = new Button("Close");
                                yes.addActionListener(ev -> {
                                    dlg.dispose();
                                });

                                dlg.add(new SpanLabel("Selected item doesn't exist!"));
                                dlg.add(FlowLayout.encloseRight(yes));
                                dlg.showPacked(BorderLayout.CENTER, true);
                            } else if (errorData.getResponseCode() == 402) {
                                rich.SubscriptionDialog();
                            }
                        })
                        .getAsString();

                if (res.getResponseCode() == 200) {
                    rich.SuccessSnackbar("Updated!");

                    new IncomeList(initForm).Home();
                }
            } catch (JSONException ex) {
            }
        }
    }
}
