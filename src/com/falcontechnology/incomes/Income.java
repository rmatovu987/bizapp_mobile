/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.incomes;

import com.codename1.ui.Image;

/**
 *
 * @author falcon
 */
public class Income {
    private Long id;
    private String date;
    private String description;
    private double revenue;
    private IncomeType type;
    private Image photo;
    private String creator;

    public Income() {
    }

    public Income(Long id, String date, String description, double revenue, IncomeType type, Image photo) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.revenue = revenue;
        this.type = type;
        this.photo = photo;
    }

    public Income(Long id, String date, String description, double revenue, IncomeType type) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.revenue = revenue;
        this.type = type;
    }

    public IncomeType getType() {
        return type;
    }

    public void setType(IncomeType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
