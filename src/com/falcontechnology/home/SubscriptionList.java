/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.home;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.SpanLabel;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class SubscriptionList {

    InitForm initForm;
    Image icon;

    public SubscriptionList(InitForm initForm) {
        this.initForm = initForm;
        fetchSubscriptions();
        fetchData();
    }

    RichComponents rich = new RichComponents();
    ArrayList<SubscriptionPojo> ShowData = new ArrayList<>();
    ArrayList<SubscriptionPojo> OriginalData = new ArrayList<>();
    InfiniteContainer List;

    ArrayList<SubPojo> subs = new ArrayList<>();

    FormFiled subscript = new FormFiled(Boolean.FALSE, "Subscription*", Boolean.TRUE, Display.PICKER_TYPE_STRINGS);
    FormFiled date = new FormFiled(Boolean.FALSE, "Start date*", Boolean.TRUE, Display.PICKER_TYPE_DATE);
    FormFiled mobile = new FormFiled(Boolean.FALSE, "Transacting mobile*");
    FormFiled money = new FormFiled(Boolean.FALSE, "Amount*");

    public void Home() {
        Form mainForm = initForm.mainForm(null, "Subscriptions", "White-Background", false, false, true);

        L10NManager lnm = L10NManager.getInstance();

        FloatingActionButton refreshbutton = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        refreshbutton.addActionListener(e -> {
            Dialog dlg = rich.Dialog("New Subscription");

            subscript.getField().addDataChangedListener((i1, i2) -> {
                System.out.println("herrrrreeeee");
                for (SubPojo i : subs) {
                    if (subscript.getContent().equals(i.getName())) {

                        System.out.println("amount: " + i.getAmount());

                        money.setContentBeforeShow(lnm.format(Double.parseDouble(i.getAmount())));
                    }
                }
            });
            mobile.setHintLabel("+256777777777", null);
            money.setEditable(false);

            Button yes = new Button("Cancel");
            yes.addActionListener(ev -> {
                dlg.dispose();
            });

            Button save = new Button("Save");
            save.addActionListener(ev -> {
                try {
                    JSONObject reqJson = new JSONObject();
                    reqJson.put("subscription", subscript.getContent());
                    reqJson.put("date", date.convertDate());
                    reqJson.put("phone", mobile.getContent());

                    Response<String> result = Rest.put(URLLinks.getMainBackend() + "business/subscription")
                            .body(reqJson.toString())
                            .acceptJson()
                            .jsonContent()
                            .bearer(initForm.data.getString("jwt"))
                            .header("token", initForm.data.getString("token"))
                            .onErrorCodeJSON((errorData) -> {
                                switch (errorData.getResponseCode()) {
                                    case 409: {
                                        Dialog dlgs = rich.Dialog("Already subscribed!");
                                        Button yess = new Button("Close");
                                        yess.addActionListener(evt -> {
                                            dlgs.dispose();
                                        });
                                        dlgs.add(new SpanLabel("Selected date is already subscribed!"));
                                        dlgs.add(FlowLayout.encloseRight(yess));
                                        dlgs.showPacked(BorderLayout.CENTER, true);
                                        break;
                                    }
                                    case 404: {
                                        Dialog dlgs = rich.Dialog("Not found!");
                                        Button yess = new Button("Close");
                                        yess.addActionListener(evt -> {
                                            dlgs.dispose();
                                        });
                                        dlgs.add(new SpanLabel("Subscription not found!"));
                                        dlgs.add(FlowLayout.encloseRight(yess));
                                        dlgs.showPacked(BorderLayout.CENTER, true);
                                        break;
                                    }
                                    case 402:
                                        rich.SubscriptionDialog();
                                        break;
                                    default:
                                        break;
                                }
                            })
                            .getAsString();

                    System.out.println("date: " + result.getResponseCode());
                    if (result.getResponseCode() == 200) {
                        rich.SuccessSnackbar("Updated!");

                        new SubscriptionList(initForm).Home();
                    }
                } catch (JSONException ek) {
                }
            });

            dlg.add(BoxLayout.encloseY(mobile.getFormField(),
                    subscript.getFormField(),
                    money.getFormField(),
                    date.getFormField()
            ));
            dlg.add(GridLayout.encloseIn(2, yes, save));
            dlg.showPacked(BorderLayout.CENTER, true);
        });
        refreshbutton.getStyle().setBgColor(ColorUtil.rgb(0, 84, 208));
        refreshbutton.bindFabToContainer(mainForm.getContentPane());
        refreshbutton.setDraggable(true);

        initForm.mainToolbar.addSearchCommand(e -> {
            String text = (String) e.getSource();
            ShowData.clear();
            if (text == null || text.length() == 0) {
                // clear search
                for (SubscriptionPojo i : OriginalData) {
                    ShowData.add(i);
                }
                mainForm.getContentPane().animateLayout(150);
            } else {
                text = text.toLowerCase();
                for (int i = 0; i < OriginalData.size(); i++) {
                    if (OriginalData.get(i).getName().toLowerCase().contains(text.toLowerCase())) {
                        ShowData.add(OriginalData.get(i));
                    }
                }
                mainForm.getContentPane().animateLayout(150);
            }
            List.refresh();
        }, 4);

        mainForm.add(BorderLayout.CENTER, IncomeList());
        mainForm.show();
    }

    private Container IncomeList() {
        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (OriginalData.isEmpty()) {
                        fetchData();
                    }
                }
                if (index + amount > ShowData.size()) {
                    amount = ShowData.size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {
                    try {
                        SubscriptionPojo acc = ShowData.get(index + itr);

                        more[itr] = SubsElement(acc);
                    } catch (JSONException ex) {
                    }
                }
                return more;
            }
        };

        return List;
    }

    private void fetchData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "business")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                OriginalData.clear();
                ShowData.clear();

                JSONArray data = new JSONArray(result.getResponseData());

                if (data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject item = data.getJSONObject(i);
                        
                        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm a");
                        
                        LocalDateTime st = LocalDateTime.parse(item.getString("startSubscription"));
                        LocalDateTime en = LocalDateTime.parse(item.getString("endSubscription"));
                        SubscriptionPojo itemx = new SubscriptionPojo(
                                item.getJSONObject("subscription").getString("name"),
                                item.getString("status"),
                                df.format(st),
                                df.format(en)
                        );
                        OriginalData.add(itemx);
                        ShowData.add(itemx);
                    }
                }
            }
        } catch (JSONException ex) {
        }
    }

    private Container SubsElement(SubscriptionPojo subss) throws JSONException {
        Label nam = new Label("Subscription: ");
        Label name = new Label(subss.getName(), "DarkGreenMedium");
        Label subs = new Label("Status: ");
        Label sub = new Label(subss.getStatus(), "DarkRedMedium");
        Label end = new Label("End: ");
        Label ed = new Label(subss.getEnd(), "LightBlueMedium");
        Label start = new Label("Start: ");
        Label strt = new Label(subss.getStart(), "ThemeBlueMedium");

        Container mm = BoxLayout.encloseY(nam, subs, start, end);
        Container nn = BoxLayout.encloseY(name, sub, strt, ed);

        return rich.Card(BoxLayout.encloseX(mm, nn));
    }

    private void fetchSubscriptions() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "business/subscription")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                ShowData.clear();

                JSONArray data = new JSONArray(result.getResponseData());

                String[] ki = new String[data.length()];
                if (data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject itemy = data.getJSONObject(i);

                        SubPojo p = new SubPojo(itemy.getString("name"), itemy.getString("amount"));
                        subs.add(p);
                        ki[i] = itemy.getString("name");

                    }
                }
                subscript.setPickerData(ki);
            }
        } catch (JSONException ex) {
        }
    }

}

class SubPojo {

    private String name;
    private String amount;

    public SubPojo(String name, String amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
