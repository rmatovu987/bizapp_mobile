/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.home;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.ChartComponent;
import com.codename1.charts.models.CategorySeries;
import com.codename1.charts.models.XYMultipleSeriesDataset;
import com.codename1.charts.models.XYSeries;
import com.codename1.charts.renderers.DefaultRenderer;
import com.codename1.charts.renderers.SimpleSeriesRenderer;
import com.codename1.charts.renderers.XYMultipleSeriesRenderer;
import com.codename1.charts.renderers.XYSeriesRenderer;
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.views.LineChart;
import com.codename1.charts.views.PieChart;
import com.codename1.charts.views.PointStyle;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import com.falcontechnology.statics.colorMixer;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class Dashboard {

    InitForm initForm;

    public Dashboard(InitForm initForm) {
        this.initForm = initForm;
        fetchSummary();
        fetchExpensesPieChart();
        fetchIncomesPieChart();
        fetchIncomeExpense();
        fetchSalePurchase();
    }
    Font many = Font.createTrueTypeFont("fontello", "fontello.ttf");
    RichComponents rich = new RichComponents();
    double incomes;
    double expenses;
    double purchases;
    double sales;
    double[] IElineExpenses = new double[12];
    double[] IElineIncomes = new double[12];
    double[] IElineNet = new double[12];
    double[] LineSales = new double[12];
    double[] LinePurchases = new double[12];
    ArrayList<PieData> expensePie = new ArrayList<>();
    ArrayList<PieData> incomePie = new ArrayList<>();

    public void Home() {
        Form mainForm = initForm.mainForm(null, "Dashboard", "White-Background", false, false, true);

        mainForm.getToolbar().add(BorderLayout.EAST, profile());

        mainForm.add(BorderLayout.CENTER, Top());
        mainForm.show();
    }

    private Button profile() {

        Button profiles = new Button("");
        Style closeStyle = profiles.getAllStyles();
        closeStyle.setPaddingUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setPadding(1, 0, 0, 1);
        profiles.setIcon(materialIcon(FontImage.MATERIAL_NOTIFICATIONS, 3, ColorUtil.WHITE));
        profiles.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                Dialog optionDialog = new Dialog("");

                Container accountoptions = new Container(BoxLayout.y());

                Button prof = new Button("My Profile");
                Container vvn = BoxLayout.encloseX(prof);
                Style closeStyle = vvn.getAllStyles();
                closeStyle.setPaddingUnit(Style.UNIT_TYPE_DIPS);
                closeStyle.setPadding(2, 2, 2, 2);
                prof.setUIID("Student-IdNo-Field");

                Button log = new Button("Logout");
                Container vvffn = BoxLayout.encloseX(log);
                Style closeStyles = vvffn.getAllStyles();
                closeStyles.setPaddingUnit(Style.UNIT_TYPE_DIPS);
                closeStyles.setPadding(2, 2, 2, 2);
                log.setUIID("Student-IdNo-Field");

                accountoptions.addAll(vvn, vvffn);

                optionDialog.add(accountoptions);

                optionDialog.showPopupDialog(profiles);
            }
        });
        return profiles;
    }

    public Container Top() {

        Container graphComponents = rich.Card(ExpensePieChart());

        Container mother = BoxLayout.encloseY(
                new Label("", "Graph-Separator"),
                TopContainer(),
                new Label("", "Graph-Separator"),
                rich.Card(PSLineGraph()),
                new Label("", "Graph-Separator"),
                graphComponents,
                new Label("", "Graph-Separator"),
                rich.Card(IncomePieChart()),
                new Label("", "Graph-Separator")
        );
        mother.setScrollableY(true);
        return mother;
    }

    private Container TopContainer() {
        Container bunny = new Container(BoxLayout.y());
        L10NManager lnm = L10NManager.getInstance();

        Label revenue = new Label("Income", "FDT-Top-Container-Labels");
        Label revenueFigure = new Label(initForm.currencyCode + " " + lnm.format(incomes), "FDT-Top-Container-Labels");
        Label revenueIcon = new Label("");
        revenueIcon.setIcon(materialIcon(FontImage.MATERIAL_ACCOUNT_BALANCE, 3, ColorUtil.WHITE));
        Container ricon = BoxLayout.encloseX(revenueIcon);
        ricon.setUIID("Purple-Icon-Card-Container");
        Container rev = BoxLayout.encloseYCenter(revenueFigure, revenue);
        rev.setUIID("Purple-Card-Container");
        Container revenues = BorderLayout.west(ricon);
        revenues.setUIID("Final-Card-Container");
        revenues.add(BorderLayout.CENTER, rev);

        Label expensesy = new Label("Expenses", "FDT-Top-Container-Labels");
        Label expensesFigure = new Label(initForm.currencyCode + " " + lnm.format(expenses), "FDT-Top-Container-Labels");
        Label expensesIcon = new Label("");
        expensesIcon.setIcon(materialIcon(FontImage.MATERIAL_EURO_SYMBOL, 3, ColorUtil.WHITE));
        Container eicon = BoxLayout.encloseX(expensesIcon);
        eicon.setUIID("Indigo-Icon-Card-Container");
        Container exp = BoxLayout.encloseYCenter(expensesFigure, expensesy);
        exp.setUIID("Indigo-Card-Container");
        Container expensess = BorderLayout.west(eicon);
        expensess.setUIID("Final-Card-Container");
        expensess.add(BorderLayout.CENTER, exp);

        Label receivables = new Label("Sales", "FDT-Top-Container-Labels");
        Label receivablesFigure = new Label(initForm.currencyCode + " " + lnm.format(sales), "FDT-Top-Container-Labels");
        Label receivablesIcon = new Label("");
        receivablesIcon.setIcon(materialIcon(FontImage.MATERIAL_MONETIZATION_ON, 3, ColorUtil.WHITE));
        Container rricon = BoxLayout.encloseX(receivablesIcon);
        rricon.setUIID("Blue-Icon-Card-Container");
        Container rec = BoxLayout.encloseYCenter(receivablesFigure, receivables);
        rec.setUIID("Blue-Card-Container");
        Container receivabless = BorderLayout.west(rricon);
        receivabless.setUIID("Final-Card-Container");
        receivabless.add(BorderLayout.CENTER, rec);

        Label payable = new Label("Purchases", "FDT-Top-Container-Labels");
        Label payableFigure = new Label(initForm.currencyCode + " " + lnm.format(purchases), "FDT-Top-Container-Labels");
        Label payableIcon = new Label("");
        payableIcon.setIcon(materialIcon(FontImage.MATERIAL_SHOPPING_CART, 3, ColorUtil.WHITE));
        Container picon = BoxLayout.encloseX(payableIcon);
        picon.setUIID("ThemeBlue-Icon-Card-Container");
        Container pay = BoxLayout.encloseYCenter(payableFigure, payable);
        pay.setUIID("ThemeBlue-Card-Container");
        Container payables = BorderLayout.west(picon);
        payables.setUIID("Final-Card-Container");
        payables.add(BorderLayout.CENTER, pay);

        bunny.add(GridLayout.encloseIn(2, receivabless, payables));
        bunny.add(GridLayout.encloseIn(2, revenues, expensess));
        bunny.add(IELineGraph());

        Container top = rich.Cardless(bunny);

        return top;
    }

    private Container IELineGraph() {
        Container LineChart = new Container(BoxLayout.y());
        LineChart.setUIID("Line-Graph-Container");

        Container revenue = new Container(BoxLayout.y()) {
            @Override
            protected Dimension calcPreferredSize() {
                Dimension d = super.calcPreferredSize();
                d.setWidth(rich.minScreensize());
                d.setHeight(Math.max(Display.getInstance().getDisplayHeight(), Display.getInstance().getDisplayWidth()) / 4);
                return d;
            }
        };
        revenue.add(createIELineGraph());

        Container top = BorderLayout.center(new Label("Income Vs Expenses", "PieChart-Title"));

        LineChart.add(top);
        LineChart.add(revenue);

        return LineChart;
    }

    private Container PSLineGraph() {
        Container LineChart = new Container(BoxLayout.y());
        LineChart.setUIID("Line-Graph-Container");

        Container revenue = new Container(BoxLayout.y()) {
            @Override
            protected Dimension calcPreferredSize() {
                Dimension d = super.calcPreferredSize();
                d.setWidth(rich.minScreensize());
                d.setHeight(Math.max(Display.getInstance().getDisplayHeight(), Display.getInstance().getDisplayWidth()) / 4);
                return d;
            }
        };
        revenue.add(createPSLineGraph());

        Container top = BorderLayout.center(new Label("Sales Vs Purchases", "PieChart-Title"));

        LineChart.add(top);
        LineChart.add(revenue);

        return LineChart;
    }

    private Container ExpensePieChart() {
        Container pieChart = new Container(BoxLayout.y());

        Container operatingExpenses = new Container(BoxLayout.y()) {
            @Override
            protected Dimension calcPreferredSize() {
                Dimension d = super.calcPreferredSize();
                d.setWidth(rich.minScreensize());
                d.setHeight(rich.minScreensize() / 2);
                return d;
            }
        };
        operatingExpenses.add(createExpensePieChartForm());

        pieChart.add(GridLayout.encloseIn(2, new Label("Expenses", "PieChart-Title")));
        pieChart.add(operatingExpenses);

        return pieChart;
    }

    private Container IncomePieChart() {
        Container pieChart = new Container(BoxLayout.y());

        Container operatingExpenses = new Container(BoxLayout.y()) {
            @Override
            protected Dimension calcPreferredSize() {
                Dimension d = super.calcPreferredSize();
                d.setWidth(rich.minScreensize());
                d.setHeight(rich.minScreensize() / 2);
                return d;
            }
        };
        operatingExpenses.add(createIncomePieChartForm());

//        Calendar cal = Calendar.getInstance();
//        Button period = new Button(new SimpleDateFormat("MMM").format(cal.getTime()));
//        period.setUIID("Period-Button");
        pieChart.add(GridLayout.encloseIn(2, new Label("Incomes", "PieChart-Title")));
        pieChart.add(operatingExpenses);

        return pieChart;
    }

    ///Expense Pie chart
    private DefaultRenderer buildExpensePieCategoryRenderer() {
        colorMixer m = new colorMixer();
        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        renderer.setMargins(new int[]{20, 30, 15, 0});
        for (int color : m.listOne) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    protected CategorySeries buildExpensePieCategoryDataset() {
        CategorySeries series = new CategorySeries("");

        if (expensePie.isEmpty()) {
            series.add("None", 100);
        } else {
            for (PieData p : expensePie) {
                series.add(p.getName(), p.getValue());
            }
        }
        return series;
    }

    public ChartComponent createExpensePieChartForm() {

        // Set up the renderer
        int[] colors = new int[]{ColorUtil.YELLOW, ColorUtil.BLUE, ColorUtil.CYAN, ColorUtil.GRAY, ColorUtil.GREEN, ColorUtil.LTGRAY, ColorUtil.MAGENTA, ColorUtil.BLACK};
        DefaultRenderer renderer = buildExpensePieCategoryRenderer();
        renderer.setLabelsTextSize(30);
        renderer.setLabelsColor(ColorUtil.BLACK);
        renderer.setLegendTextSize(30);
        renderer.setDisplayValues(false);
        renderer.setInScroll(false);
        renderer.setStartAngle(270);
        renderer.setShowLabels(true);
        renderer.setShowLegend(false);

        // Create the chart ... pass the values and renderer to the chart object.
        PieChart chart = new PieChart(buildExpensePieCategoryDataset(), renderer);

        // Wrap the chart in a Component so we can add it to a form
        ChartComponent c = new ChartComponent(chart);

        return c;
    }

    //Income Pie chart
    private DefaultRenderer buildIncomePieCategoryRenderer() {
        colorMixer m = new colorMixer();
        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        renderer.setMargins(new int[]{20, 30, 15, 0});
        for (int color : m.listTwo) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    protected CategorySeries buildIncomePieCategoryDataset() {
        CategorySeries series = new CategorySeries("");

        if (incomePie.isEmpty()) {
            series.add("None", 100);
        } else {
            for (PieData p : incomePie) {
                series.add(p.getName(), p.getValue());
            }
        }
        return series;
    }

    public ChartComponent createIncomePieChartForm() {

        // Set up the renderer
        DefaultRenderer renderer = buildIncomePieCategoryRenderer();
        renderer.setLabelsTextSize(30);
        renderer.setLabelsColor(ColorUtil.BLACK);
        renderer.setLegendTextSize(30);
        renderer.setDisplayValues(false);
        renderer.setInScroll(false);
        renderer.setStartAngle(270);
        renderer.setShowLabels(true);
        renderer.setShowLegend(false);

        // Create the chart ... pass the values and renderer to the chart object.
        PieChart chart = new PieChart(buildIncomePieCategoryDataset(), renderer);

        // Wrap the chart in a Component so we can add it to a form
        ChartComponent c = new ChartComponent(chart);

        return c;
    }

    ///IE Line graph
    private ChartComponent createIELineGraph() {
        String[] titles = new String[]{"Expenses", "Revenue", "Net Income"};

        ArrayList<double[]> x = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            x.add(new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
        }

        ArrayList<double[]> values = new ArrayList<>();
        values.add(IElineExpenses);
        values.add(IElineIncomes);
        values.add(IElineNet);

        int[] colors = new int[]{ColorUtil.BLUE, ColorUtil.GREEN, ColorUtil.MAGENTA};
        PointStyle[] styles = new PointStyle[]{PointStyle.CIRCLE, PointStyle.DIAMOND,
            PointStyle.TRIANGLE, PointStyle.SQUARE};

        XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles);
        int length = renderer.getSeriesRendererCount();
        for (int i = 0; i < length; i++) {
            ((XYSeriesRenderer) renderer.getSeriesRendererAt(i)).setFillPoints(true);
        }

        ///chart settings
        renderer.setChartTitle("");
        renderer.setXTitle("Month");
        renderer.setYTitle("Cash (millions)");
        renderer.setXAxisMin(0.5);
        renderer.setXAxisMax(12.5);
        renderer.setYAxisMin(0);
        renderer.setYAxisMax(20);
        initRendererer(renderer);

        renderer.setPanEnabled(false);
        renderer.setXLabels(12);
        renderer.setYLabels(10);;
        renderer.setXLabelsAlign(Component.CENTER);
        renderer.setYLabelsAlign(Component.RIGHT);

        renderer.addXTextLabel(1, "Jan");
        renderer.addXTextLabel(2, "Feb");
        renderer.addXTextLabel(3, "Mar");
        renderer.addXTextLabel(4, "Apr");
        renderer.addXTextLabel(5, "May");
        renderer.addXTextLabel(6, "Jun");
        renderer.addXTextLabel(7, "Jul");
        renderer.addXTextLabel(8, "Aug");
        renderer.addXTextLabel(9, "Sep");
        renderer.addXTextLabel(10, "Oct");
        renderer.addXTextLabel(11, "Nov");
        renderer.addXTextLabel(12, "Dec");

        LineChart chart = new LineChart(buildDataset(titles, x, values), renderer);

        ChartComponent c = new ChartComponent(chart);
        return c;
    }
    
    ///PS Line graph
    private ChartComponent createPSLineGraph() {
        String[] titles = new String[]{"Sale", "Purchases"};

        ArrayList<double[]> x = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            x.add(new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
        }

        ArrayList<double[]> values = new ArrayList<>();
        values.add(LinePurchases);
        values.add(LineSales);

        int[] colors = new int[]{ColorUtil.BLUE, ColorUtil.MAGENTA};
        PointStyle[] styles = new PointStyle[]{PointStyle.CIRCLE, PointStyle.DIAMOND,
            PointStyle.TRIANGLE, PointStyle.SQUARE};

        XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles);
        int length = renderer.getSeriesRendererCount();
        for (int i = 0; i < length; i++) {
            ((XYSeriesRenderer) renderer.getSeriesRendererAt(i)).setFillPoints(true);
        }

        ///chart settings
        renderer.setChartTitle("");
        renderer.setXTitle("Month");
        renderer.setYTitle("Cash (millions)");
        renderer.setXAxisMin(0.5);
        renderer.setXAxisMax(12.5);
        renderer.setYAxisMin(0);
        renderer.setYAxisMax(20);
        initRendererer(renderer);

        renderer.setPanEnabled(false);
        renderer.setXLabels(12);
        renderer.setYLabels(10);;
        renderer.setXLabelsAlign(Component.CENTER);
        renderer.setYLabelsAlign(Component.RIGHT);

        renderer.addXTextLabel(1, "Jan");
        renderer.addXTextLabel(2, "Feb");
        renderer.addXTextLabel(3, "Mar");
        renderer.addXTextLabel(4, "Apr");
        renderer.addXTextLabel(5, "May");
        renderer.addXTextLabel(6, "Jun");
        renderer.addXTextLabel(7, "Jul");
        renderer.addXTextLabel(8, "Aug");
        renderer.addXTextLabel(9, "Sep");
        renderer.addXTextLabel(10, "Oct");
        renderer.addXTextLabel(11, "Nov");
        renderer.addXTextLabel(12, "Dec");

        LineChart chart = new LineChart(buildDataset(titles, x, values), renderer);

        ChartComponent c = new ChartComponent(chart);
        return c;
    }

      
    protected XYMultipleSeriesDataset buildDataset(String[] titles, ArrayList<double[]> xValues, ArrayList<double[]> yValues) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        addXYSeries(dataset, titles, xValues, yValues, 0);
        return dataset;
    }

    public void addXYSeries(XYMultipleSeriesDataset dataset, String[] titles, ArrayList<double[]> xValues, ArrayList<double[]> yValues, int scale) {
        int length = titles.length;
        for (int i = 0; i < length; i++) {
            XYSeries series = new XYSeries(titles[i], scale);
            double[] xV = xValues.get(i);
            double[] yV = yValues.get(i);
            int seriesLength = xV.length;
            for (int k = 0; k < seriesLength; k++) {
                series.add(xV[k], yV[k]);
            }

            dataset.addSeries(series);
        }
    }

    protected XYMultipleSeriesRenderer buildRenderer(int[] colors, PointStyle[] styles) {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        setRenderer(renderer, colors, styles);
        return renderer;
    }
    
    protected void setRenderer(XYMultipleSeriesRenderer renderer, int[] colors, PointStyle[] styles) {
        renderer.setAxisTitleTextSize(smallFont.getHeight() / 3);
        renderer.setChartTitleTextSize(smallFont.getHeight());
        renderer.setLabelsTextSize(smallFont.getHeight() / 3);
        renderer.setLegendTextSize(smallFont.getHeight() / 3);
        renderer.setPointSize(5f);
        renderer.setMargins(new int[]{20, 20, 10, 20});
        int length = colors.length;
        for (int i = 0; i < length; i++) {
            XYSeriesRenderer r = new XYSeriesRenderer();
            r.setColor(colors[i]);
            r.setPointStyle(styles[i]);
            renderer.addSeriesRenderer(r);
        }
    }

    protected void initRendererer(DefaultRenderer renderer) {
        renderer.setBackgroundColor(ColorUtil.WHITE);
        renderer.setApplyBackgroundColor(true);
        renderer.setLabelsColor(ColorUtil.BLACK);
        renderer.setAxesColor(ColorUtil.BLACK);
        if (Font.isNativeFontSchemeSupported()) {
            Font fnt = Font.createTrueTypeFont("native:MainLight", "native:MainLight").
                    derive(Display.getInstance().convertToPixels(1.5f), Font.STYLE_PLAIN);
            renderer.setTextTypeface(fnt);
            renderer.setChartTitleTextFont(fnt);
            renderer.setLabelsTextFont(fnt);
            renderer.setLegendTextFont(fnt);

            if (renderer instanceof XYMultipleSeriesRenderer) {
                ((XYMultipleSeriesRenderer) renderer).setAxisTitleTextFont(fnt);
            }
            if (renderer instanceof XYMultipleSeriesRenderer) {
                XYMultipleSeriesRenderer x = (XYMultipleSeriesRenderer) renderer;
                x.setMarginsColor(ColorUtil.WHITE);
                x.setXLabelsColor(ColorUtil.BLACK);
                x.setYLabelsColor(0, ColorUtil.BLACK);
            }
        }

    }

    Font smallFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.SIZE_SMALL, Font.STYLE_PLAIN);
    Font medFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.SIZE_MEDIUM, Font.STYLE_PLAIN);

    private void fetchSummary() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "dashboard/summary")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                JSONObject data = new JSONObject(result.getResponseData());

                expenses = data.getDouble("expenses");
                incomes = data.getDouble("incomes");
                sales = data.getDouble("sales");
                purchases = data.getDouble("purchases");

            }
        } catch (JSONException ex) {
        }
    }

    private void fetchExpensesPieChart() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "dashboard/pieExpenses")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                JSONArray data = new JSONArray(result.getResponseData());

                for (int i = 0; i < data.length(); i++) {
                    JSONObject e = data.getJSONObject(i);

                    PieData p = new PieData(e.getString("name"), e.getDouble("value"));
                    if (p.getValue() != 0) {
                        expensePie.add(p);
                    }
                }

            }
        } catch (JSONException ex) {
        }
    }

    private void fetchIncomesPieChart() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "dashboard/pieIncomes")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                JSONArray data = new JSONArray(result.getResponseData());

                for (int i = 0; i < data.length(); i++) {
                    JSONObject e = data.getJSONObject(i);

                    PieData p = new PieData(e.getString("name"), e.getDouble("value"));
                    if (p.getValue() != 0) {
                        incomePie.add(p);
                    }
                }

            }
        } catch (JSONException ex) {
        }
    }

    private void fetchIncomeExpense() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "dashboard/netincome")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                JSONArray data = new JSONArray(result.getResponseData());

                for (int i = 0; i < data.length(); i++) {
                    JSONObject itm = data.getJSONObject(i); 
                  
                    IElineExpenses[i] = itm.getDouble("expenses");
                    IElineIncomes[i] = itm.getDouble("incomes");
                    IElineNet[i] = itm.getDouble("net");
                    
                }
            }

        } catch (JSONException ex) {
        }
    }
    
    private void fetchSalePurchase() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "dashboard/salepurchase")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                JSONArray data = new JSONArray(result.getResponseData());

                for (int i = 0; i < data.length(); i++) {
                    JSONObject itm = data.getJSONObject(i); 
                   
                    LinePurchases[i] = itm.getDouble("purchase");
                    LineSales[i] = itm.getDouble("sale");
                    
                }
            }

        } catch (JSONException ex) {
        }
    }
}

class PieData {

    private String name;
    private double value;

    public PieData(String name, double value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
