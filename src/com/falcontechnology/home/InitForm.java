/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.home;

import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.ImageViewer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.auth.LoginForm;
import com.falcontechnology.expenses.ExpenseList;
import com.falcontechnology.incomes.IncomeList;
import com.falcontechnology.items.ItemList;
import com.falcontechnology.purchases.PurchaseList;
import com.falcontechnology.reports.ReportList;
import com.falcontechnology.sales.SalesList;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import com.falcontechnology.users.User;
import com.falcontechnology.users.NewUser;

/**
 *
 * @author falcon
 */
public class InitForm {

    public Resources theme;
    public JSONObject data;
    public JSONObject userData;
    public JSONObject business;
    public String subscription;
    public JSONObject userRole;
    public Toolbar mainToolbar;
    public User user;
    public String currencyCode;
    
    public InitForm(Resources theme, JSONObject data) {
        this.theme = theme;
        this.data = data;
        try {
            this.userData = data.getJSONObject("user");
        } catch (JSONException ex) {
        }
        try {
            this.business = data.getJSONObject("business");
        } catch (JSONException ex) {
        }
        try {
            this.subscription = data.getJSONObject("business").getJSONObject("latestSubscription").getString("status");
        } catch (JSONException ex) {
        }
        try {
            this.userRole = data.getJSONObject("userRole").getJSONObject("role");
        } catch (JSONException ex) {
        }
        try {
            this.currencyCode = data.getJSONObject("business").getJSONObject("currency").getString("code");
        } catch (JSONException ex) {
        }

        Image icon;
        int screensize = Math.round(Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight()));
        try {
            ConnectionRequest.setDefaultUserAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0");
            JSONObject url = userData.getJSONObject("photo");
            Image defaultImage = theme.getImage("user.png").scaled(screensize / 4, screensize / 4);
            EncodedImage placeholder = (EncodedImage) defaultImage;
            icon = URLImage.createToStorage(placeholder, "User" + data.getJSONObject("user").getString("id"), url.getString("url"));

        } catch (JSONException e) {
            icon = theme.getImage("avater.png").scaled(screensize / 4, screensize / 4);
        }

        User userr = new User();
        try {
            userr.setEmail(data.getJSONObject("user").getString("email"));
            userr.setFirstName(data.getJSONObject("user").getString("firstName"));
            userr.setId(Long.parseLong(data.getJSONObject("user").getString("id")));
            userr.setLastName(data.getJSONObject("user").getString("lastName"));
            if (data.getJSONObject("user").getString("otherName") != null) {
                userr.setOtherName(data.getJSONObject("user").getString("otherName"));
            } else {
                userr.setOtherName("");
            }
            userr.setPhone(data.getJSONObject("user").getString("phoneNumber"));
            userr.setPhoto(icon);
        } catch (JSONException | NumberFormatException e) {
        }
        this.user = userr;

    }

    RichComponents rich = new RichComponents();

    public void Home() {
        Form form = new Form("", new BorderLayout());
        form.setScrollableY(true);

        mainToolbar = new Toolbar();
        form.setToolbar(mainToolbar);

        ToolbarGeneral(mainToolbar);

        Dashboard dashboard = new Dashboard(this);
        dashboard.Home();
    }

    public void ToolbarGeneral(Toolbar mainToolbar) {
        int screensize = Math.round(Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight()));

        Image icon = user.getPhoto();
        Image roundMask = Image.createImage(screensize / 4, screensize / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillArc(0, 0, screensize / 4, screensize / 4, 0, 360);
        Object mask = roundMask.createMask();
        icon = icon.applyMask(mask);
        Label logoimage = new Label("", "Avater-Border");
        logoimage.setIcon(icon);

        Button uploadPhoto = new Button("", "SideMenu-EditUser-icon");
        uploadPhoto.setIcon(materialIcon(FontImage.MATERIAL_EDIT, 2, ColorUtil.alpha(0x0052cc)));

        uploadPhoto.addActionListener(evt -> {
            new NewUser(this, true, "SideMenu", user).Home();
        });

        Container logoimageCnt = BoxLayout.encloseXCenter(logoimage);
        Style logoStyle = logoimageCnt.getAllStyles();
        logoStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        logoStyle.setMargin(0, 0, 0, 0);

        Container photobox = BoxLayout.encloseY(LayeredLayout.encloseIn(logoimageCnt,
                FlowLayout.encloseRightBottom(uploadPhoto)));

        Container companycontainer = new Container(BoxLayout.x());
        companycontainer.setUIID("Company");
        try {
            companycontainer.add(new Label(business.getString("name"), "SideMenu-TopArea-CompanyName"));
        } catch (JSONException ex) {
        }

        Container role = new Container(BoxLayout.x());
        role.setUIID("Title");
        try {
            role.add(new Label(userRole.getString("name"), "SideMenu-TopArea-UserName"));
        } catch (JSONException ex) {
        }

        Container roleBox = new Container(BoxLayout.y());
        roleBox.add(BorderLayout.centerAbsolute(role));

        Container usercontainer = new Container(BoxLayout.x());
        usercontainer.setUIID("Title");
        usercontainer.add(new Label(user.getFirstName() + " " + user.getLastName() + " " + user.getOtherName(), "SideMenu-TopArea-UserName"));

        Container userBox = new Container(BoxLayout.y());
        userBox.add(BorderLayout.centerAbsolute(usercontainer));

        Container avatarBox = BorderLayout.centerAbsolute(photobox);
        avatarBox.setUIID("SideMenu-TopArea-ComponentsBox");
        avatarBox.add(BorderLayout.SOUTH, BorderLayout.centerAbsolute(BoxLayout.encloseYCenter(userBox, roleBox, companycontainer)));

        mainToolbar.addComponentToSideMenu(avatarBox);
        mainToolbar.addMaterialCommandToSideMenu(" Home", FontImage.MATERIAL_HOME, e -> {
            Dashboard mdas = new Dashboard(this);
            mdas.Home();
        });
        mainToolbar.addMaterialCommandToSideMenu(" Items", FontImage.MATERIAL_STORE, e -> {
            if (subscription.equals("EXPIRED")) {
                rich.SubscriptionDialog();
            } else {
                new ItemList(this).Home();
            }
        });
        mainToolbar.addMaterialCommandToSideMenu(" Sales", FontImage.MATERIAL_MONETIZATION_ON, e -> {
            if (subscription.equals("EXPIRED")) {
                rich.SubscriptionDialog();
            } else {
                new SalesList(this).Home();
            }
        });
        mainToolbar.addMaterialCommandToSideMenu(" Purchases", FontImage.MATERIAL_SHOPPING_CART, e -> {
            if (subscription.equals("EXPIRED")) {
                rich.SubscriptionDialog();
            } else {
                new PurchaseList(this).Home();
            }
        });
        mainToolbar.addMaterialCommandToSideMenu(" Incomes", FontImage.MATERIAL_ACCOUNT_BALANCE, e -> {
            if (subscription.equals("EXPIRED")) {
                rich.SubscriptionDialog();
            } else {
                new IncomeList(this).Home();
            }
        });
        mainToolbar.addMaterialCommandToSideMenu(" Expenses", FontImage.MATERIAL_EURO_SYMBOL, e -> {
            if (subscription.equals("EXPIRED")) {
                rich.SubscriptionDialog();
            } else {
                new ExpenseList(this).Home();
            }
        });
        mainToolbar.addMaterialCommandToSideMenu(" Reports", FontImage.MATERIAL_REPORT, e -> {
            if (subscription.equals("EXPIRED")) {
                rich.SubscriptionDialog();
            } else {
                new ReportList(this).Home();
            }
        });
        mainToolbar.addMaterialCommandToSideMenu(" Settings", FontImage.MATERIAL_SECURITY, e -> new SettingsPage(this).Home());
        mainToolbar.addMaterialCommandToSideMenu(" Logout", FontImage.MATERIAL_POWER_SETTINGS_NEW, e -> logout());

        Image logox = theme.getImage("logo.png").scaledWidth(screensize / 2);
        Label LogoImageBox = new Label(logox);
        Container logoBox = new Container(BoxLayout.y());
        logoBox.add(BorderLayout.centerAbsolute(LogoImageBox));
        logoBox.add(new Label("Simplified Financial Management", "SideMenu-TopArea-Slogan"));
        logoBox.add(new Label("", "Commons-Space"));
        logoBox.setUIID("SideMenu-TopArea-Logo");
        mainToolbar.setComponentToSideMenuSouth(logoBox);

        mainToolbar.getLeftSideMenuButton().setUIID("Toolbar-Hambugger");
    }

    public void logout() {
        try {
            Response<String> result = Rest.post(URLLinks.getMainBackend() + "auth/logout")
                    .jsonContent()
                    .bearer(data.getString("jwt"))
                    .header("token", data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                new LoginForm(theme).show();
            }
        } catch (JSONException ex) {
        }
    }

    /**
     * Create a form with a toolbar
     *
     * @param FormHeaderEditing the name/title of the form if editing
     * @param FormHeaderNew the name/title of the form when new
     * @param FormUIID the UIID of the form
     * @param backbutton whether you need a back-button or not.
     * @param editing whether you are in editing mode or not
     * @param centerTitle whether the title should be centered or not
     * @return a newly created form
     */
    public Form mainForm(String FormHeaderEditing, String FormHeaderNew, String FormUIID,
            boolean backbutton, boolean editing, boolean centerTitle) {

        Form mainForm = new Form(new BorderLayout());
        mainForm.setUIID(FormUIID);
        mainForm.setScrollableY(false);

        mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        mainForm.setToolbar(mainToolbar);

        if (backbutton == false) {
            try {
                ToolbarGeneral(mainToolbar);
            } catch (Exception e) {
            }
        } else {
            Form previous = Display.getInstance().getCurrent();
            mainToolbar.setBackCommand("", e -> previous.showBack());
        }

        if (centerTitle == false) {
            Container TittleContainer = new Container(new BorderLayout());

            if (editing) {
                Label toobarLabel = new Label(FormHeaderEditing, "Toolbar-HeaderLabel");
                TittleContainer.add(toobarLabel);
            } else {
                Label toobarLabel = new Label(FormHeaderNew, "Toolbar-HeaderLabel");
                TittleContainer.add(toobarLabel);
            }
            mainToolbar.setTitleComponent(TittleContainer);
        } else {

            if (editing) {
                mainToolbar.setTitleComponent(BorderLayout.center(new Label(FormHeaderEditing, "Toolbar-HeaderLabel")));

            } else {
                mainToolbar.setTitleComponent(BorderLayout.center(new Label(FormHeaderNew, "Toolbar-HeaderLabel")));
            }

        }
        return mainForm;
    }

    public Form subForm(String FormHeaderEditing, String FormHeaderNew, String FormUIID,
            boolean editing, boolean centerTitle) {

        Form mainForm = new Form(new BorderLayout());
        mainForm.setUIID(FormUIID);
        mainForm.setScrollableY(false);

        mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        mainForm.setToolbar(mainToolbar);

        if (centerTitle == false) {
            Container TittleContainer = BoxLayout.encloseX();

            if (editing) {
                Label toobarLabel = new Label(FormHeaderEditing, "Toolbar-HeaderLabel");
                TittleContainer.add(toobarLabel);
            } else {
                Label toobarLabel = new Label(FormHeaderNew, "Toolbar-HeaderLabel");
                TittleContainer.add(toobarLabel);
            }
            mainToolbar.setTitleComponent(TittleContainer);
        } else {
            if (editing) {
                mainToolbar.setTitleComponent(BorderLayout.center(new Label(FormHeaderEditing, "Toolbar-HeaderLabel")));

            } else {
                mainToolbar.setTitleComponent(BorderLayout.center(new Label(FormHeaderNew, "Toolbar-HeaderLabel")));
            }
        }
        return mainForm;
    }

    public Form ImageViewer(Image i) {

        Form mainForm = new Form(new BorderLayout());
        mainForm.setUIID("Blue-Form-Background");
        mainForm.setScrollableY(false);

        mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        mainForm.setToolbar(mainToolbar);

        Form previous = Display.getInstance().getCurrent();
        mainToolbar.setBackCommand("", e -> previous.showBack());

        ImageViewer iv = new ImageViewer(i);

        mainForm.add(BorderLayout.CENTER, iv);

        return mainForm;
    }
}
