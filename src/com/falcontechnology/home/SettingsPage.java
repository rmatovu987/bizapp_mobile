/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.home;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.capture.Capture;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.Log;
import com.codename1.io.Storage;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.getCurrentForm;
import static com.codename1.ui.CN.isPortrait;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.GeneralPath;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.ImageIO;
import com.codename1.util.Base64;
import com.codename1.util.OnComplete;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import com.falcontechnology.users.Role;
import com.falcontechnology.users.User;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class SettingsPage {

    InitForm initForm;
    Label logoimage;
    Image icon;
    String userImage64;

    public SettingsPage(InitForm initForm) {
        this.initForm = initForm;
        if (!initForm.subscription.equals("EXPIRED")) {
            fetchUsers();
        }
    }

    RichComponents rich = new RichComponents();

    public void Home() {
        try {
            Form mainform = initForm.mainForm(null, "Settings", "Blue-Form-Background", false, false, true);

            loadCountries();

            Container Companymother = BorderLayout.center(LayeredLayout.encloseIn(businessdetails(),
                    FlowLayout.encloseCenter(TopContainer())));

            Container buff = BoxLayout.encloseY(Companymother, Subscription(), Users());
            buff.setScrollableY(true);

            mainform.add(BorderLayout.CENTER, buff);

            mainform.show();
        } catch (JSONException ex) {
        }
    }

    private Container TopContainer() throws JSONException {

        Container logoimageCnt = BoxLayout.encloseXCenter(pic());
        Style logoStyle = logoimageCnt.getAllStyles();
        logoStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        logoStyle.setMargin(0, 0, 0, 0);

        Container photobox = BoxLayout.encloseY(LayeredLayout.encloseIn(logoimageCnt,
                FlowLayout.encloseRightBottom(updatePhotoBtn())));

        Container avatarBox = BorderLayout.centerAbsolute(photobox);
        avatarBox.setUIID("Top-Container");

        return avatarBox;
    }

    private Container pic() throws JSONException {
        int screensize = Math.round(Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight()));

        JSONObject url = initForm.business.getJSONObject("logo");
        Image defaultImage = initForm.theme.getImage("biz.jpeg").scaled(screensize / 4, screensize / 4);
        EncodedImage placeholder = (EncodedImage) defaultImage;
        icon = URLImage.createToStorage(placeholder, "Business" + initForm.business.getString("id"), url.getString("url"));

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);
        Object mask = roundMask.createMask();
        icon = icon.applyMask(mask);
        logoimage = new Label("", "Avater-Border");
        logoimage.setIcon(icon);

        Container logoimageCnt = BoxLayout.encloseXCenter(logoimage);

        return logoimageCnt;
    }

    private Button updatePhotoBtn() {

        Button uploadPhoto = new Button("", "SideMenu-EditUser-icon");
        uploadPhoto.setIcon(materialIcon(FontImage.MATERIAL_CAMERA_ALT, 3, ColorUtil.alpha(0x0052cc)));

        uploadPhoto.addActionListener(evt -> {
            String i = Capture.capturePhoto();
            if (i != null) {

                try {
                    final Image newImage = Image.createImage(i);
                    Image roundedMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
                    Graphics gra = roundedMask.getGraphics();
                    gra.setColor(0xffffff);
                    gra.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);

                    Object masked = roundedMask.createMask();

                    cropImage(newImage, rich.minScreensize() / 4, rich.minScreensize() / 4, et -> {

                        try {
                            ImageIO img = ImageIO.getImageIO();
                            ByteArrayOutputStream out = new ByteArrayOutputStream();
                            img.save(et, out, ImageIO.FORMAT_JPEG, 1);
                            byte[] ba = out.toByteArray();
                            userImage64 = Base64.encode(ba);
                            et = et.applyMask(masked);

                            logoimage.setIcon(et);
                            try {
                                Storage.getInstance().deleteStorageFile("Business" + initForm.business.getString("id"));

                                JSONObject reqJson = new JSONObject();
                                reqJson.put("fileName", "Image" + String.valueOf(System.currentTimeMillis()));
                                reqJson.put("content", userImage64);

                                Response<String> result = Rest.put(URLLinks.getMainBackend() + "business/photo/" + initForm.data.getString("id"))
                                        .body(reqJson.toString())
                                        .jsonContent()
                                        .bearer(initForm.data.getString("jwt"))
                                        .header("token", initForm.data.getString("token"))
                                        .onErrorCodeJSON((errorData) -> {
                                            if (errorData.getResponseCode() == 400) {
                                                Dialog dlg = rich.Dialog("Not found!");

                                                Button yes = new Button("Close");
                                                yes.addActionListener(ev -> {
                                                    dlg.dispose();
                                                });

                                                dlg.add(new SpanLabel("Content empty!"));
                                                dlg.add(FlowLayout.encloseRight(yes));
                                                dlg.showPacked(BorderLayout.CENTER, true);
                                            } else if (errorData.getResponseCode() == 402) {
                                                rich.SubscriptionDialog();
                                            }
                                        })
                                        .getAsString();

                                if (result.getResponseCode() == 200) {

                                    rich.SuccessSnackbar("Photo updated!");

                                    new SettingsPage(initForm).Home();
                                }
                            } catch (Exception e) {
                            }
                            logoimage.getComponentForm().revalidate();
                        } catch (IOException ex) {

                        }

                    });

                } catch (IOException ex) {
                    Log.p("Error loading captured image from camera", Log.ERROR);

                }
            }
        });

        return uploadPhoto;
    }

    private void cropImage(Image img, int destWidth, int destHeight, OnComplete<Image> s) {

        Form previous = getCurrentForm();
        Form cropForm = new Form("", new LayeredLayout());

        Label toobarLabel = new Label("New Holder", "Toolbar-HeaderLabel");
        cropForm.setTitleComponent(toobarLabel);

        Toolbar mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        cropForm.setToolbar(mainToolbar);

        Label moveAndZoom = new Label("Move and zoom the photo to crop it");
        moveAndZoom.getUnselectedStyle().setFgColor(0xffffff);
        moveAndZoom.getUnselectedStyle().setAlignment(CENTER);
        moveAndZoom.setCellRenderer(true);
        cropForm.setGlassPane((Graphics g, Rectangle rect) -> {
            g.setColor(0x0000ff);
            g.setAlpha(150);
            Container cropCp = cropForm.getContentPane();
            int posY = cropForm.getContentPane().getAbsoluteY();

            GeneralPath p = new GeneralPath();
            p.setRect(new Rectangle(0, posY, cropCp.getWidth(), cropCp.getHeight()), null);
            if (isPortrait()) {
                p.arc(0, posY + cropCp.getHeight() / 2 - cropCp.getWidth() / 2,
                        cropCp.getWidth() - 1, cropCp.getWidth() - 1, 0, Math.PI * 2);
            } else {
                p.arc(cropCp.getWidth() / 2 - cropCp.getHeight() / 2, posY,
                        cropCp.getHeight() - 1, cropCp.getHeight() - 1, 0, Math.PI * 2);
            }
            g.fillShape(p);
            g.setAlpha(255);
            g.setColor(0xffffff);
            moveAndZoom.setX(0);
            moveAndZoom.setY(posY);
            moveAndZoom.setWidth(cropCp.getWidth());
            moveAndZoom.setHeight(moveAndZoom.getPreferredH());
            moveAndZoom.paint(g);
        });

        final ImageViewer viewer = new ImageViewer();
        viewer.setImage(img);

        cropForm.add(viewer);
        cropForm.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_CROP, e -> {
            previous.showBack();
            s.completed(viewer.getCroppedImage(0).
                    fill(destWidth, destHeight));
        });
        cropForm.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_CANCEL, e -> previous.showBack());
        cropForm.show();

    }

    FormFiled countrys = new FormFiled(Boolean.FALSE, "Country*", Boolean.TRUE, Display.PICKER_TYPE_STRINGS);
    FormFiled currencys = new FormFiled(Boolean.FALSE, "Currency*", Boolean.TRUE, Display.PICKER_TYPE_STRINGS);
    FormFiled businessname = new FormFiled(Boolean.FALSE, "Business Name*");

    private Container businessdetails() throws JSONException {
        Label name = new Label(initForm.business.getString("name"), "ThemeBlueBig");
        Label status = new Label("Status: ", "LightBlueBig");
        Label stat = new Label(initForm.business.getString("status"), "DarkRedMedium");
        Label country = new Label("Country: ", "LightBlueBig");
        Label ctry = new Label(initForm.business.getString("country"), "DarkRedMedium");
        Label currency = new Label("Currency: ", "LightBlueBig");
        Label curr = new Label(initForm.business.getJSONObject("currency").getString("name"), "DarkRedMedium");

        Button newholder = new Button("Edit");
        newholder.setIcon(materialIcon(FontImage.MATERIAL_EDIT, 2, 0x0052cc));
        newholder.setUIID("BlueLink");
        newholder.addActionListener(e -> {
            Dialog dlg = rich.Dialog("Edit business details");

            try {
                businessname.setContentBeforeShow(initForm.business.getString("name"));
                currencys.setSelectedPickerString(initForm.business.getJSONObject("currency").getString("name"));
                countrys.setSelectedPickerString(initForm.business.getString("country"));
            } catch (Exception en) {
            }
            Button yes = new Button("Cancel");
            yes.addActionListener(ev -> {
                dlg.dispose();
            });

            Button save = new Button("Update");
            save.addActionListener(ev -> {
                try {
                    JSONObject reqJson = new JSONObject();
                    reqJson.put("country", countrys.getContent());
                    reqJson.put("currency", currencys.getContent());
                    reqJson.put("businessName", businessname.getContent());

                    Response<String> result = Rest.put(URLLinks.getMainBackend() + "business")
                            .body(reqJson.toString())
                            .acceptJson()
                            .jsonContent()
                            .bearer(initForm.data.getString("jwt"))
                            .header("token", initForm.data.getString("token"))
                            .getAsString();

                    if (result.getResponseCode() == 200) {
                        rich.SuccessSnackbar("Updated!");

                        new SettingsPage(initForm).Home();
                    }
                } catch (JSONException ek) {
                }
            });

            dlg.add(BoxLayout.encloseY(
                    businessname.getFormField(),
                    countrys.getFormField(),
                    currencys.getFormField()
            ));
            dlg.add(GridLayout.encloseIn(2, yes, save));
            dlg.showPacked(BorderLayout.CENTER, true);
        });

        Container c = BoxLayout.encloseYCenter(
                BoxLayout.encloseXCenter(name),
                BoxLayout.encloseXCenter(status, stat),
                BoxLayout.encloseXCenter(country, ctry),
                BoxLayout.encloseXCenter(currency, curr),
                BoxLayout.encloseXRight(newholder)
        );
        c.setUIID("Top-Settings-Container");

        return c;
    }

    private Container Subscription() throws JSONException {
        Label sublabel = new Label("Subscription", "Settings-Label");

        Container top = BorderLayout.center(sublabel);
        top.setUIID("Setting-Label-Container");

        Label subs = new Label("Subscription: ", "LightBlueMedium");
        Label sub = new Label(initForm.business.getJSONObject("latestSubscription").getString("status"), "DarkGreenMedium");
        Label end = new Label("End: ", "LightBlueMedium");
        Label ed = new Label(initForm.business.getJSONObject("latestSubscription").getString("endSubscription"), "DarkGreenMedium");
        Label start = new Label("Start: ", "LightBlueMedium");
        Label strt = new Label(initForm.business.getJSONObject("latestSubscription").getString("startSubscription"), "DarkGreenMedium");

        Button newholder = new Button("View more...");
        newholder.setIcon(materialIcon(FontImage.MATERIAL_SUBSCRIPTIONS, 2, 0x0052cc));
        newholder.setUIID("BlueLink");
        newholder.addActionListener(e -> {
            new SubscriptionList(initForm).Home();
        });

        Container mm = BoxLayout.encloseY(subs, end, start);
        Container nn = BoxLayout.encloseY(sub, strt, ed);
        Container c = BoxLayout.encloseYCenter(
                top,
                BoxLayout.encloseX(mm, nn),
                BoxLayout.encloseXRight(newholder)
        );
        c.setUIID("Settings-Container");

        return c;
    }

    private Container Users() throws JSONException {
        Label sublabel = new Label("Users", "Settings-Label");

        FormFiled firstName = new FormFiled(false, "First name*");
        FormFiled lastName = new FormFiled(false, "Last name*");
        FormFiled otherName = new FormFiled(false, "Other name");
        FormFiled phone = new FormFiled(false, "Phone number*");
        phone.setIsPhone(Boolean.TRUE);
        FormFiled email = new FormFiled(false, "Email*");
        email.setIsEmail(Boolean.TRUE);

        Button addUser = new Button("", "Button-Call");
        addUser.setIcon(materialIcon(FontImage.MATERIAL_ADD, 3, 0x0052cc));
        addUser.addActionListener(fff -> {
            Dialog dlg = rich.Dialog("New User");

            Button yes = new Button("Close");
            yes.addActionListener(ev -> {
                dlg.dispose();
            });

            Button save = new Button("Save");
            save.addActionListener(ev -> {
                try {
                    JSONObject req = new JSONObject();
                    req.put("firstName", firstName.getContent());
                    req.put("lastName", lastName.getContent());
                    req.put("otherName", otherName.getContent());
                    req.put("phoneNumber", phone.getContent());
                    req.put("email", email.getContent());

                    Response<String> result = Rest.post(URLLinks.getMainBackend() + "users")
                            .body(req.toString())
                            .acceptJson()
                            .jsonContent()
                            .bearer(initForm.data.getString("jwt"))
                            .header("token", initForm.data.getString("token"))
                            .getAsString();

                    if (result.getResponseCode() == 201) {
                        rich.SuccessSnackbar("Saved!");

                        new SettingsPage(initForm).Home();
                    }
                } catch (JSONException e) {
                }
            });

            dlg.add(BoxLayout.encloseY(
                    firstName.getFormField(),
                    lastName.getFormField(),
                    otherName.getFormField(),
                    phone.getFormField(),
                    email.getFormField())
            );
            dlg.add(GridLayout.encloseIn(2, yes, save));
            dlg.showPacked(BorderLayout.CENTER, true);
        });

        Container top = BorderLayout.center(sublabel);
        if (!initForm.subscription.equals("EXPIRED")) {
            top.add(BorderLayout.EAST, addUser);
        }
        top.setUIID("Setting-Label-Container");

        Container c = new Container(BoxLayout.yCenter());
        c.add(top);
        if (!initForm.subscription.equals("EXPIRED")) {
            c.add(UsersList());
        }
        c.setUIID("Settings-Container");
        c.setScrollableY(false);

        return c;
    }

    InfiniteContainer List;
    ArrayList<User> ShowData = new ArrayList<>();

    private Container UsersList() {

        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (ShowData.isEmpty()) {
//                        fetchUsers();
                    }
                }
                if (index + amount > ShowData.size()) {
                    amount = ShowData.size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {
                    User acc = ShowData.get(index + itr);
                    more[itr] = Users(acc);
                }
                return more;
            }
        };

        return List;
    }

    private void fetchUsers() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "users")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                ShowData.clear();

                JSONArray data = new JSONArray(result.getResponseData());

                if (data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject itemy = data.getJSONObject(i);
                        JSONObject user = itemy.getJSONObject("user");
                        JSONObject role = itemy.getJSONObject("role");
                        try {
                            JSONObject url = user.getJSONObject("photo");
                            Image defaultImage = initForm.theme.getImage("avater.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
                            EncodedImage placeholder = (EncodedImage) defaultImage;

                            icon = URLImage.createToStorage(placeholder, "User" + user.getString("id"), url.getString("url"));

                        } catch (JSONException e) {
                            icon = initForm.theme.getImage("user.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
                        }

                        String itt = "";
                        if (user.getString("otherName") != null) {
                            itt = user.getString("otherName");
                        }

                        User itemx = new User(
                                user.getLong("id"),
                                user.getString("firstName"),
                                user.getString("lastName"),
                                itt,
                                itemy.getString("status"),
                                user.getString("phoneNumber"),
                                user.getString("email"),
                                new Role(role.getLong("id"), role.getString("name")),
                                icon
                        );
                        ShowData.add(itemx);
                    }
                }
            }
        } catch (JSONException ex) {
        }
    }

    private Container Users(User user) {
        Image photoIcon = user.getPhoto();

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillRoundRect(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 10, 10);
        Object mask = roundMask.createMask();
        photoIcon = photoIcon.applyMaskAutoScale(mask);

        Button photo = new Button(photoIcon);
        Style closeStyle = photo.getAllStyles();
        closeStyle.setFgColor(0xffffff);
        closeStyle.setBgTransparency(0);
        closeStyle.setPaddingUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setPadding(1, 1, 1, 1);
        closeStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setMargin(0, 0, 0, 3);

        Label name = new Label(user.getFirstName() + " " + user.getLastName() + " " + user.getOtherName());
//        name.setUIID("List-Line1");
        name.getAllStyles().setAlignment(Component.LEFT);

        Label role = new Label(user.getRole().getName());
        role.setUIID("DarkGreenMedium");
        role.getAllStyles().setAlignment(Component.LEFT);

        Label phone = new Label(user.getPhone());
        phone.setUIID("LightBlueMedium");
        phone.getAllStyles().setAlignment(Component.LEFT);

        Label email = new Label(user.getEmail());
        email.setUIID("ThemeBlueMedium");
        email.getAllStyles().setAlignment(Component.LEFT);

        Label status = new Label(user.getStatus());
        status.setUIID("RedMedium");
        status.getAllStyles().setAlignment(Component.LEFT);

        Button call = new Button("", "Button-Call");
        call.setIcon(materialIcon(FontImage.MATERIAL_CALL, 2, 0x0052cc));
        call.addActionListener(re -> {
            Display.getInstance().dial(user.getPhone());
        });

        Button options = new Button("", "Button-Call");
        options.setIcon(materialIcon(FontImage.MATERIAL_DRAG_INDICATOR, 2, 0x0052cc));
        options.addActionListener(re -> {
            Dialog dlg = rich.Dialog(user.getFirstName() + "'s Options");

            Button makeAdmin = rich.blueButton("Make Admin");

            Button activate = rich.blueButton("Activate");

            Button deactivate = rich.blueButton("Deactivate");

            if (user.getStatus().equals("ACTIVE")) {
                dlg.add(BoxLayout.encloseY(makeAdmin, deactivate));
            } else {
                dlg.add(BoxLayout.encloseY(makeAdmin, activate));
            }

            dlg.setDisposeWhenPointerOutOfBounds(true);
            dlg.showPacked(BorderLayout.CENTER, true);
        });

        Container top = BorderLayout.center(phone);
        top.add(BorderLayout.EAST, call);

        Container top1 = BorderLayout.center(name);
        top1.add(BorderLayout.EAST, options);

        Container lines = BorderLayout.west(photo);
        lines.add(BorderLayout.CENTER, BoxLayout.encloseYCenter(top1, role, top, email, status));

        return rich.UnPaddedCard(lines);
    }

    private void loadCountries() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "auth").acceptJson().contentType("application/json").getAsString();

            if (result.getResponseCode() == 200) {
                JSONArray data = new JSONArray(result.getResponseData());
                String[] countries = new String[data.length()];
                String[] currencies = new String[data.length()];

                for (int i = 0; i < data.length(); i++) {
                    JSONObject item = data.getJSONObject(i);
                    countries[i] = item.getString("country");
                    currencies[i] = item.getString("name");
                }
                countrys.setPickerData(countries);
                currencys.setPickerData(currencies);
            }
        } catch (JSONException ex) {
        }
    }

}
