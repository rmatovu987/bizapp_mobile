/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.purchases;

import com.codename1.ui.Image;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class Purchase {
    private String id;
    private String date;
    private String receiptNumber;
    private Image photo;
    private ArrayList<PurchaseItem> items;
    private double totalIncome;

    public Purchase() {
    }

    public Purchase(String id, String date, String receiptNumber, Image photo, ArrayList<PurchaseItem> items) {
        this.id = id;
        this.date =date;
        this.receiptNumber = receiptNumber;
        this.photo = photo;
        this.items = items;
        
        double tot = 0.0;
        for(PurchaseItem i : items){
            tot = tot +i.getCostPrice();
        }
        this.totalIncome = tot;
    }

    public double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public ArrayList<PurchaseItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<PurchaseItem> items) {
        this.items = items;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
