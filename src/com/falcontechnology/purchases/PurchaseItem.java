/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.purchases;

import com.falcontechnology.items.Item;

/**
 *
 * @author falcon
 */
public class PurchaseItem {
    private String id;
    private String date;
    private double quantity;
    private Item item;
    private double costPrice;
    private String creator;

    public PurchaseItem() {
    }

    public PurchaseItem(String id, double quantity, Item item, double costPrice) {
        this.id = id;
        this.quantity = quantity;
        this.item = item;
        this.costPrice = costPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(double costPrice) {
        this.costPrice = costPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
