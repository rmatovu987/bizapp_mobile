/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.purchases;

import ca.weblite.codename1.json.JSONArray;
import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.items.Item;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.util.ArrayList;

/**
 *
 * @author falcon
 */
public class PurchaseList {

    InitForm initForm;
    InfiniteContainer List;
    Image icon;

    public PurchaseList(InitForm initForm) {
        this.initForm = initForm;
    }

    RichComponents rich = new RichComponents();
    ArrayList<Purchase> ShowData = new ArrayList<>();
    ArrayList<Purchase> OriginalData = new ArrayList<>();

    public void Home() {

        Form mainForm = initForm.mainForm(null, "Purchases", "White-Background", false, false, true);

        FloatingActionButton refreshbutton = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        refreshbutton.addActionListener(e -> {
            new NewPurchase(initForm, false, new Purchase()).Home();
        });
        refreshbutton.getStyle().setBgColor(ColorUtil.rgb(0, 84, 208));
        refreshbutton.bindFabToContainer(mainForm.getContentPane());
        refreshbutton.setDraggable(true);

        initForm.mainToolbar.addSearchCommand(e -> {
            String text = (String) e.getSource();
            ShowData.clear();
            if (text == null || text.length() == 0) {
                // clear search
                for (Purchase i : OriginalData) {
                    ShowData.add(i);
                }
                mainForm.getContentPane().animateLayout(150);
            } else {
                text = text.toLowerCase();
                for (int i = 0; i < OriginalData.size(); i++) {
                    if (OriginalData.get(i).getReceiptNumber().toLowerCase().contains(text.toLowerCase())) {
                        ShowData.add(OriginalData.get(i));
                    }
                }
                mainForm.getContentPane().animateLayout(150);
            }
            List.refresh();
        }, 4);

        mainForm.add(BorderLayout.CENTER, ItemList());
        mainForm.show();
    }

    public Container ItemList() {

        List = new InfiniteContainer() {

            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    if (OriginalData.isEmpty()) {
                        fetchData();
                    }
                }
                if (index + amount > ShowData.size()) {
                    amount = ShowData.size() - index;
                    if (amount <= 0) {
                        return null;
                    }
                }

                Component[] more = new Component[amount];
                for (int itr = 0; itr < amount; itr++) {

                    try {
                        boolean iseven = false;
                        iseven = itr % 2 == 0;
                        Purchase acc = ShowData.get(index + itr);
                        MultiButton mb = new MultiButton();
                        mb.add(BorderLayout.CENTER, SaleElement(acc, iseven));

                        mb.addActionListener(e -> {
                            new NewPurchase(initForm, true, acc).Home();
                        });
                        more[itr] = mb;
                    } catch (JSONException ex) {
                    }
                }
                return more;
            }
        };

        return List;
    }

    private void fetchData() {
        try {
            Response<String> result = Rest.get(URLLinks.getMainBackend() + "purchases")
                    .jsonContent()
                    .bearer(initForm.data.getString("jwt"))
                    .header("token", initForm.data.getString("token"))
                    .getAsString();

            if (result.getResponseCode() == 200) {
                OriginalData.clear();
                ShowData.clear();

                JSONArray data = new JSONArray(result.getResponseData());

                if (data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject item = data.getJSONObject(i);
                        try {
                            JSONObject url = item.getJSONObject("attachment");
                            Image defaultImage = initForm.theme.getImage("purchase.png").scaled(rich.minScreensize() / 4, rich.minScreensize() / 4);
                            EncodedImage placeholder = (EncodedImage) defaultImage;

                            icon = URLImage.createToStorage(placeholder, "Purchase" + item.getString("id"), url.getString("url"));

                        } catch (JSONException e) {
                        }

                        ArrayList<PurchaseItem> items = new ArrayList<>();
                        JSONArray sales = item.getJSONArray("purchaseItems");

                        for (int j = 0; j < sales.length(); j++) {
                            JSONObject dd = sales.getJSONObject(j);
                            PurchaseItem h = new PurchaseItem(
                                    dd.getString("id"),
                                    dd.getDouble("quantity"),
                                    new Item(
                                            dd.getJSONObject("item").getString("id"),
                                            dd.getJSONObject("item").getString("name"),
                                            dd.getJSONObject("item").getString("description"),
                                            dd.getJSONObject("item").getInt("quantityInStock"),
                                            dd.getJSONObject("item").getBoolean("trackQuantity")
                                    ),
                                    dd.getDouble("costPrice")
                            );
                            items.add(h);
                        }

                        Purchase itemx = new Purchase(
                                item.getString("id"),
                                item.getString("date"),
                                item.getString("receiptNumber"),
                                icon,
                                items
                        );
                        OriginalData.add(itemx);
                        ShowData.add(itemx);
                    }
                }
            }
        } catch (JSONException ex) {
        }
    }

    public Container SaleElement(Purchase jdata, boolean iseven) throws JSONException {
        Image photoIcon = jdata.getPhoto();

        L10NManager lnm = L10NManager.getInstance();

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillRoundRect(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 10, 10);
        Object mask = roundMask.createMask();
        photoIcon = photoIcon.applyMaskAutoScale(mask);

        Button photo = new Button(photoIcon);
        Style closeStyle = photo.getAllStyles();
        closeStyle.setFgColor(0xffffff);
        closeStyle.setBgTransparency(0);
        closeStyle.setPaddingUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setPadding(1, 1, 1, 1);
        closeStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        closeStyle.setMargin(0, 0, 0, 1);

        Label receiptnumber = new Label("Receipt: " + jdata.getReceiptNumber());
        receiptnumber.setUIID("List-Line11");
        receiptnumber.getAllStyles().setAlignment(Component.LEFT);

        Label date = new Label(jdata.getDate());
        date.setUIID("List-Line22");
        date.getAllStyles().setAlignment(Component.LEFT);

        Label quantity = new Label("Income: ");
        quantity.setUIID("List-Line33");
        quantity.getAllStyles().setAlignment(Component.RIGHT);

        Label ty = new Label(initForm.business.getJSONObject("currency").getString("code") + " " + lnm.format(jdata.getTotalIncome()));
        ty.setUIID("Red-Pill");
        ty.getAllStyles().setAlignment(Component.CENTER);

        Container photoContainer = BoxLayout.encloseYCenter(new Label(photoIcon));
        photoContainer.getAllStyles().setMarginUnit(Style.UNIT_TYPE_DIPS);
        photoContainer.getAllStyles().setMarginRight(3);

        Container lines = BorderLayout.west(photoContainer);
        lines.add(BorderLayout.CENTER, BoxLayout.encloseYCenter(receiptnumber, date, BoxLayout.encloseX(quantity, ty)));

        return rich.UnPaddedCard(lines);
    }
}
