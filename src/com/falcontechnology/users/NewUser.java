/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.users;

import ca.weblite.codename1.json.JSONException;
import ca.weblite.codename1.json.JSONObject;
import com.codename1.capture.Capture;
import com.codename1.charts.util.ColorUtil;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.Log;
import com.codename1.io.Storage;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.getCurrentForm;
import static com.codename1.ui.CN.isPortrait;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.geom.GeneralPath;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.ImageIO;
import com.codename1.util.Base64;
import com.codename1.util.OnComplete;
import static com.falcontechnology.MyApplication.materialIcon;
import com.falcontechnology.auth.LoginForm;
import com.falcontechnology.home.Dashboard;
import com.falcontechnology.home.InitForm;
import com.falcontechnology.statics.FormFiled;
import com.falcontechnology.statics.RichComponents;
import com.falcontechnology.statics.URLLinks;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 *
 * @author rmato
 */
public class NewUser {

    InitForm initForm;
    boolean editing;
    String origin;
    User user;

    public NewUser(InitForm initForm, boolean editing, String origin, User user) {
        this.initForm = initForm;
        this.editing = editing;
        this.origin = origin;
        this.user = user;
    }

    String userImage64 = "";
    Label logoimage;

    RichComponents rich = new RichComponents();

    public void Home() {

        Form mainForm = initForm.subForm("User details", "New User", "Blue-Form-Background", editing, true);

        initForm.mainToolbar.addCommandToLeftBar("", materialIcon(FontImage.MATERIAL_CHEVRON_LEFT, 3, 0xffffff), e -> {
            if ("SideMenu".equals(origin)) {
                new Dashboard(initForm).Home();
            }
        });

        Container Companymother = BorderLayout.center(LayeredLayout.encloseIn(businessdetails(),
                FlowLayout.encloseCenter(TopContainer())));

        mainForm.add(BorderLayout.CENTER, Companymother);
        mainForm.show();

    }

    //photo and update button
    private Container TopContainer() {

        Container logoimageCnt = BoxLayout.encloseXCenter(UserPhoto());
        Style logoStyle = logoimageCnt.getAllStyles();
        logoStyle.setMarginUnit(Style.UNIT_TYPE_DIPS);
        logoStyle.setMargin(0, 0, 0, 0);

        Container photobox = BoxLayout.encloseY(LayeredLayout.encloseIn(logoimageCnt,
                FlowLayout.encloseRightBottom(updatePhotoBtn())));

        Container avatarBox = BorderLayout.centerAbsolute(photobox);
        avatarBox.setUIID("Top-Container");

        return avatarBox;
    }

    //bottom container
    public Container businessdetails() {

        Container details = new Container(BoxLayout.y());
        details.setScrollableY(true);
        details.addAll(UserName(), UserEmail());
        if (editing) {
            details.add(UserPassword());
        }

        details.setUIID("Bottom-Container");

        return details;
    }

    //user photo
    public Container UserPhoto() {
        Image icon = user.getPhoto();

        Image roundMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
        Graphics gr = roundMask.getGraphics();
        gr.setColor(0xffffff);
        gr.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);
        Object mask = roundMask.createMask();
        icon = icon.applyMask(mask);
        logoimage = new Label("", "Avater-Border");
        logoimage.setIcon(icon);

        Container logoimageCnt = BoxLayout.encloseXCenter(logoimage);

        return logoimageCnt;
    }

    public Button updatePhotoBtn() {

        Button uploadPhoto = new Button("", "SideMenu-EditUser-icon");
        uploadPhoto.setIcon(materialIcon(FontImage.MATERIAL_CAMERA_ALT, 3, ColorUtil.alpha(0x0052cc)));

        uploadPhoto.addActionListener(evt -> {
            String i = Capture.capturePhoto();
            if (i != null) {

                try {
                    final Image newImage = Image.createImage(i);
                    Image roundedMask = Image.createImage(rich.minScreensize() / 4, rich.minScreensize() / 4, 0xff000000);
                    Graphics gra = roundedMask.getGraphics();
                    gra.setColor(0xffffff);
                    gra.fillArc(0, 0, rich.minScreensize() / 4, rich.minScreensize() / 4, 0, 360);

                    Object masked = roundedMask.createMask();

                    cropImage(newImage, rich.minScreensize() / 4, rich.minScreensize() / 4, et -> {

                        if (editing) {
                            try {
                                ImageIO img = ImageIO.getImageIO();
                                ByteArrayOutputStream out = new ByteArrayOutputStream();
                                img.save(et, out, ImageIO.FORMAT_JPEG, 1);
                                byte[] ba = out.toByteArray();
                                userImage64 = Base64.encode(ba);
                                et = et.applyMask(masked);

                                logoimage.setIcon(et);
                                try {
                                    Storage.getInstance().deleteStorageFile("User" + user.getId());

                                    JSONObject reqJson = new JSONObject();
                                    reqJson.put("fileName", "Image" + String.valueOf(System.currentTimeMillis()));
                                    reqJson.put("content", userImage64);

                                    Response<String> result = Rest.put(URLLinks.getMainBackend() + "users/photo/" + user.getId())
                                            .body(reqJson.toString())
                                            .jsonContent()
                                            .bearer(initForm.data.getString("jwt"))
                                            .header("token", initForm.data.getString("token"))
                                            .pathParam("id", String.valueOf(user.getId()))
                                            .onErrorCodeJSON((errorData) -> {
                                                if (errorData.getResponseCode() == 404) {
                                                    Dialog dlg = rich.Dialog("Not found!");

                                                    Button yes = new Button("Close");
                                                    yes.addActionListener(ev -> {
                                                        dlg.dispose();
                                                    });

                                                    dlg.add(new SpanLabel("Selected user doesn't exist!"));
                                                    dlg.add(FlowLayout.encloseRight(yes));
                                                    dlg.showPacked(BorderLayout.CENTER, true);
                                                } else if (errorData.getResponseCode() == 402) {
                                                    rich.SubscriptionDialog();
                                                }
                                            })
                                            .getAsString();

                                    if (result.getResponseCode() == 200) {
                                        if (user.getId().equals(initForm.user.getId())) {
                                            initForm.logout();
                                        } else {
                                            rich.SuccessSnackbar("Photo updated!");

                                            Home();
                                            /////change to go to users list
                                        }
                                    }
                                } catch (Exception e) {
                                }
                                logoimage.getComponentForm().revalidate();
                            } catch (IOException ex) {

                            }
                        } else {
                            et = et.applyMask(masked);

                            logoimage.setIcon(et);
                        }

                    });

                } catch (IOException ex) {
                    Log.p("Error loading captured image from camera", Log.ERROR);

                }
            }
        });

        return uploadPhoto;
    }

    private void cropImage(Image img, int destWidth, int destHeight, OnComplete<Image> s) {

        Form previous = getCurrentForm();
        Form cropForm = new Form("", new LayeredLayout());

        Label toobarLabel = new Label("New Holder", "Toolbar-HeaderLabel");
        cropForm.setTitleComponent(toobarLabel);

        Toolbar mainToolbar = new Toolbar();
        mainToolbar.setUIID("ToolBar");
        cropForm.setToolbar(mainToolbar);

        Label moveAndZoom = new Label("Move and zoom the photo to crop it");
        moveAndZoom.getUnselectedStyle().setFgColor(0xffffff);
        moveAndZoom.getUnselectedStyle().setAlignment(CENTER);
        moveAndZoom.setCellRenderer(true);
        cropForm.setGlassPane((Graphics g, Rectangle rect) -> {
            g.setColor(0x0000ff);
            g.setAlpha(150);
            Container cropCp = cropForm.getContentPane();
            int posY = cropForm.getContentPane().getAbsoluteY();

            GeneralPath p = new GeneralPath();
            p.setRect(new Rectangle(0, posY, cropCp.getWidth(), cropCp.getHeight()), null);
            if (isPortrait()) {
                p.arc(0, posY + cropCp.getHeight() / 2 - cropCp.getWidth() / 2,
                        cropCp.getWidth() - 1, cropCp.getWidth() - 1, 0, Math.PI * 2);
            } else {
                p.arc(cropCp.getWidth() / 2 - cropCp.getHeight() / 2, posY,
                        cropCp.getHeight() - 1, cropCp.getHeight() - 1, 0, Math.PI * 2);
            }
            g.fillShape(p);
            g.setAlpha(255);
            g.setColor(0xffffff);
            moveAndZoom.setX(0);
            moveAndZoom.setY(posY);
            moveAndZoom.setWidth(cropCp.getWidth());
            moveAndZoom.setHeight(moveAndZoom.getPreferredH());
            moveAndZoom.paint(g);
        });

        final ImageViewer viewer = new ImageViewer();
        viewer.setImage(img);

        cropForm.add(viewer);
        cropForm.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_CROP, e -> {
            previous.showBack();
            s.completed(viewer.getCroppedImage(0).
                    fill(destWidth, destHeight));
        });
        cropForm.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_CANCEL, e -> previous.showBack());
        cropForm.show();

    }

    //name
    private Container UserName() {
        Container userdetails = new Container(BoxLayout.y());

        Label profileLabel = new Label("User information", "HeaderLabel");

        FormFiled firstname = new FormFiled(Boolean.FALSE, "First Name");
        FormFiled lastname = new FormFiled(Boolean.FALSE, "Last Name");
        FormFiled othername = new FormFiled(Boolean.FALSE, "Other Name");
        FormFiled phone = new FormFiled(Boolean.FALSE, "Phone number");

        try {
            firstname.setContentBeforeShow(user.getFirstName());
            lastname.setContentBeforeShow(user.getLastName());
            othername.setContentBeforeShow(user.getOtherName());
            phone.setContentBeforeShow(user.getPhone());
        } catch (Exception e) {
        }

        Button update = rich.blueButton("Update");
        update.addActionListener(p -> {
            try {
                user.setFirstName(firstname.getContent());
                user.setLastName(lastname.getContent());
                user.setOtherName(othername.getContent());
                user.setPhone(phone.getContent());
                backendItem(false);
            } catch (Exception e) {
            }
        });

        userdetails.addAll(
                profileLabel,
                firstname.getFormField(),
                lastname.getFormField(),
                othername.getFormField(),
                phone.getFormField()
        );

        if (editing) {
            userdetails.add(update);
        }

        Container name = rich.Card(userdetails);

        return name;
    }

    //email
    private Container UserEmail() {
        Container email = new Container(BoxLayout.y());

        Label editLabel = new Label("User email (Not editable)", "HeaderLabel");

        FormFiled emailLogin = new FormFiled(Boolean.FALSE, "Email");
        emailLogin.setIsEmail(true);
        emailLogin.setEditable(Boolean.FALSE);
        try {
            emailLogin.setContentBeforeShow(user.getEmail());
        } catch (Exception e) {
        }

        email.addAll(editLabel, emailLogin.getFormField());

        Container emaill = rich.Card(email);

        return emaill;
    }

    //password
    private Container UserPassword() {
        Container password = new Container(BoxLayout.yCenter());

        Label passLabel = new Label("Edit password", "HeaderLabel");

        FormFiled oldpassword = new FormFiled(Boolean.FALSE, "Old Password");
        oldpassword.setIsPassword(true);

        FormFiled newpassword = new FormFiled(Boolean.FALSE, "New Password");
        newpassword.setIsPassword(true);

        FormFiled confirmpassword = new FormFiled(Boolean.FALSE, "Confirm Password");
        confirmpassword.setIsPassword(true);

        Button save = rich.blueButton("Save");
        save.addActionListener(e -> {
            if (newpassword.getContent().equals(confirmpassword.getContent())) {
                try {
                    JSONObject reqJson = new JSONObject();
                    reqJson.put("password", newpassword.getContent());
                    reqJson.put("oldpassword", oldpassword.getContent());

                    Response<String> result = Rest.put(URLLinks.getMainBackend() + "users/credentials")
                            .body(reqJson.toString())
                            .jsonContent()
                            .bearer(initForm.data.getString("jwt"))
                            .header("token", initForm.data.getString("token"))
                            .onErrorCodeJSON((errorData) -> {
                                switch (errorData.getResponseCode()) {
                                    case 402:
                                        rich.SubscriptionDialog();
                                        break;

                                    case 409:
                                        rich.FailureSnackbar("Wrong credentials!");
                                        break;

                                    default:
                                        break;
                                }
                            })
                            .getAsString();

                    if (result.getResponseCode() == 200) {
                        rich.SuccessSnackbar("Credentials updated!");

                        initForm.logout();
                    }
                } catch (JSONException ex) {
                }
            } else {
                rich.FailureSnackbar("Passwords are not matching!");
            }
        });

        password.addAll(passLabel, oldpassword.getFormField(), newpassword.getFormField(), confirmpassword.getFormField(),
                save);

        Container pass = rich.Card(password);

        return pass;
    }

    private void backendItem(boolean save) throws IOException {
        if (save) {
            try {
                JSONObject image = new JSONObject();
                if (null != user.getPhoto()) {
                    ImageIO img = ImageIO.getImageIO();
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    img.save(user.getPhoto(), out, ImageIO.FORMAT_JPEG, 1);
                    byte[] ba = out.toByteArray();
                    String images = Base64.encode(ba);

                    image.put("fileName", "Image" + String.valueOf(System.currentTimeMillis()));
                    image.put("content", images);
                }

                JSONObject reqJson = new JSONObject();
                reqJson.put("firstName", user.getFirstName());
                reqJson.put("lastName", user.getLastName());
                reqJson.put("otherName", user.getOtherName());
                reqJson.put("phoneNumber", user.getPhone());
                reqJson.put("email", user.getEmail());
                reqJson.put("photo", image);

                Response<String> result = Rest.post(URLLinks.getMainBackend() + "users")
                        .body(reqJson.toString())
                        .jsonContent()
                        .bearer(initForm.data.getString("jwt"))
                        .header("token", initForm.data.getString("token"))
                        .onErrorCodeJSON((errorData) -> {
                            if (errorData.getResponseCode() == 409) {
                                Dialog dlg = rich.Dialog("Conflict");

                                Button yes = new Button("Close");
                                yes.addActionListener(ev -> {
                                    dlg.dispose();
                                });

                                dlg.add(new SpanLabel("User already exists!"));
                                dlg.add(FlowLayout.encloseRight(yes));
                                dlg.showPacked(BorderLayout.CENTER, true);
                            } else if (errorData.getResponseCode() == 402) {
                                rich.SubscriptionDialog();
                            } else if (errorData.getResponseCode() == 406) {
                                Dialog dlg = rich.Dialog("Unauthorized");

                                Button yes = new Button("Close");
                                yes.addActionListener(ev -> {
                                    dlg.dispose();
                                });

                                dlg.add(new SpanLabel("Ask the business administrator to perform this action!"));
                                dlg.add(FlowLayout.encloseRight(yes));
                                dlg.showPacked(BorderLayout.CENTER, true);
                            } else if (errorData.getResponseCode() == 304) {
                                Dialog dlg = rich.Dialog("Not found");

                                Button yes = new Button("Close");
                                yes.addActionListener(ev -> {
                                    dlg.dispose();
                                });

                                dlg.add(new SpanLabel("Role user not found!"));
                                dlg.add(FlowLayout.encloseRight(yes));
                                dlg.showPacked(BorderLayout.CENTER, true);
                            }
                        })
                        .getAsString();

                if (result.getResponseCode() == 201) {
                    rich.SuccessSnackbar("User created!");

                    new UserList(initForm).Home();
                }
            } catch (JSONException ex) {
            }
        } else {
            try {
                JSONObject reqJson = new JSONObject();
                reqJson.put("firstName", user.getFirstName());
                reqJson.put("lastName", user.getLastName());
                reqJson.put("otherName", user.getOtherName());
                reqJson.put("phoneNumber", user.getPhone());

                Response<String> res = Rest.put(URLLinks.getMainBackend() + "users/" + user.getId())
                        .body(reqJson.toString())
                        .jsonContent()
                        .bearer(initForm.data.getString("jwt"))
                        .header("token", initForm.data.getString("token"))
                        .pathParam("id", String.valueOf(user.getId()))
                        .onErrorCodeJSON((errorData) -> {
                            if (errorData.getResponseCode() == 404) {
                                Dialog dlg = rich.Dialog("Not found!");

                                Button yes = new Button("Close");
                                yes.addActionListener(ev -> {
                                    dlg.dispose();
                                });

                                dlg.add(new SpanLabel("Selected user doesn't exist!"));
                                dlg.add(FlowLayout.encloseRight(yes));
                                dlg.showPacked(BorderLayout.CENTER, true);
                            } else if (errorData.getResponseCode() == 402) {
                                rich.SubscriptionDialog();
                            }
                        })
                        .getAsString();

                if (res.getResponseCode() == 200) {
                    rich.SuccessSnackbar("User updated!");
                    Home();
//                    new UserList(initForm).Home();
                }
            } catch (JSONException ex) {
            }
        }
    }
}
