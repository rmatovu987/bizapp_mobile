/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.statics;

import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.animations.CommonTransitions;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.RoundBorder;
import com.falcontechnology.home.InitForm;

/**
 *
 * @author falcon
 */
public class RichComponents {
    
    public Container EmptyGeneralPlaceHolder(InitForm initForm, String message) {

        Image io = initForm.theme.getImage("empty.png").scaled(minScreensize() / 6, minScreensize() / 6);

        Label talk = new Label(message, "List-emptyLabel");
        Container placeholder = new Container(BoxLayout.y());
        placeholder.add(BorderLayout.centerAbsolute(new Label(io)));
        placeholder.add(BorderLayout.centerAbsolute(talk));

        return placeholder;
    }

    /**
     * Create a rich button
     *
     * @param label the label to appear on the button
     *
     * @return a newly created button
     */
    public Button blueButton(String label) {
        Button next = new Button(label);
        next.setUIID("Login-Button");
        next.getStyle().setBorder(RoundBorder.create().rectangle(true).color(0x0052cc));

        return next;
    }

    /**
     * Create a rich word button
     *
     * @param blueBackground whether the button is going to sit on a blue
     * background or not
     * @param name the name or word that is going to appear in the button
     *
     * @return a newly created word button
     */
    public Button wordButton(boolean blueBackground, String name) {
        Button button = new Button(name);
        if (blueBackground != true) {
            button.setUIID("Word-Button");
        } else {
            button.setUIID("Word-Button-Blue");
        }

        return button;
    }
    
    /**
     * Create a rich card
     *
     * @param card the well-laid final container that is to appear in the rich
     * card.
     *
     * @return a newly created card
     */
    public Container Card(Container card) {
        Container cardContainer = new Container(BoxLayout.y());
        cardContainer.setUIID("Card");
        cardContainer.add(card);
        return cardContainer;
    }
    
    public Container UnPaddedCard(Container card) {
        Container cardContainer = new Container(BoxLayout.y());
        cardContainer.setUIID("UnPaddedCard");
        cardContainer.add(card);
        return cardContainer;
    }

    public Container Cardless(Container card) {
        Container cardContainer = new Container(BoxLayout.y());
        cardContainer.setUIID("Cardless");
        cardContainer.add(card);
        return cardContainer;
    }

    /**
     * Create a rich interactive dialog box.
     *
     * @param title the title of the dialog box
     *
     * @return a newly created dialog box
     */
    public Dialog Dialog(String title) {
        Dialog dlg = new Dialog();
        if (title != null) {
            dlg.setTitleComponent(new Label(title, "Dialog-Title"));
        }
        dlg.setLayout(BoxLayout.y());
        dlg.setDialogUIID("Dialog-UIID");
        dlg.getContentPane().setUIID("Dialog-ContentPane");
        dlg.setTransitionInAnimator(CommonTransitions.createFade(1000));
        dlg.setTransitionOutAnimator(CommonTransitions.createFade(800));
        dlg.setBlurBackgroundRadius(-1);

        return dlg;
    }

    public Dialog MandatoryDialog() {
        Dialog dlg = Dialog("Field Error");

        Button yes = new Button("Close");
        yes.addActionListener(ev -> {
            dlg.dispose();
        });

        dlg.add(new SpanLabel("Fields with * can't be left empty!"));
        dlg.add(FlowLayout.encloseRight(yes));
        dlg.showPacked(BorderLayout.CENTER, true);
        return dlg;
    }

    public Dialog SubscriptionDialog() {
        Dialog dlg = Dialog("Payment required!");

        Button yes = new Button("Close");
        yes.addActionListener(ev -> {
            dlg.dispose();
        });

        dlg.add(new SpanLabel("Your subscription has expired. Go to settings and subscribe!"));
        dlg.add(FlowLayout.encloseRight(yes));
        dlg.showPacked(BorderLayout.CENTER, true);
        
        return dlg;
    }

    /**
     * Create a rich SnackBar for events when successful.
     *
     * @param message the message to be displayed in the SnackBar.
     *
     * @return a newly created SnackBar
     */
    public ToastBar.Status SuccessSnackbar(String message) {
        ToastBar.Status status = ToastBar.getInstance().createStatus();
        status.setMessage(message);
        status.setExpires(5000);
        status.setUiid("Success-Snackbar");
        status.setMessageUIID("Snackbar-Message");
        status.show();

        return status;
    }

    /**
     * Create a rich SnackBar for events when unsuccessful.
     *
     * @param message the message to be displayed in the SnackBar.
     *
     * @return a newly created SnackBar
     */
    public ToastBar.Status FailureSnackbar(String message) {
        ToastBar.Status status = ToastBar.getInstance().createStatus();
        status.setMessage(message);
        status.setExpires(5000);
        status.setUiid("Failure-Snackbar");
        status.setMessageUIID("Snackbar-Message");
        status.show();

        return status;
    }

    /**
     * Create a rich SnackBar for events that are warning.
     *
     * @param message the message to be displayed in the SnackBar.
     *
     * @return a newly created SnackBar
     */
    public ToastBar.Status WarningSnackbar(String message) {
        ToastBar.Status status = ToastBar.getInstance().createStatus();
        status.setMessage(message);
        status.setExpires(10000);
        status.setUiid("Warning-Snackbar");
        status.setMessageUIID("Snackbar-Message");
        status.show();

        return status;
    }

    /**
     * Create a rich SnackBar for events that are warning.
     *
     * @param message the message to be displayed in the SnackBar.
     *
     * @return a newly created SnackBar
     */
    public ToastBar.Status LoadingSnackbar(String message) {
        ToastBar.Status status = ToastBar.getInstance().createStatus();
        status.setMessage(message);
//        status.setExpires(10000);
        status.setUiid("Loading-Snackbar");
        status.setMessageUIID("Snackbar-Message");
        status.show();

        return status;
    }

    /**
     * Size of screen width.
     *
     * @return the width of the screen
     */
    public int minScreensize() {

        int screensize = Math.round(Math.min(Display.getInstance().getDisplayHeight(), Display.getInstance().getDisplayWidth()));

        return screensize;
    }

    /**
     * Size of screen height.
     *
     * @return the height of the screen
     */
    public int maxScreensize() {

        int screensize = Math.round(Math.max(Display.getInstance().getDisplayHeight(), Display.getInstance().getDisplayWidth()));

        return screensize;
    }

}
