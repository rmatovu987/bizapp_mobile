/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.falcontechnology.statics;

import com.codename1.charts.util.ColorUtil;
import java.util.ArrayList;

public class colorMixer {

    public ArrayList<Integer> listOne = new ArrayList<>();

    public ArrayList<Integer> listTwo = new ArrayList<>();

    public colorMixer() {

        listOne.add(ColorUtil.rgb(252, 219, 3));
        listOne.add(ColorUtil.rgb(3, 252, 23));
        listOne.add(ColorUtil.rgb(194, 3, 252));
        listOne.add(ColorUtil.rgb(3, 236, 252));
        listOne.add(ColorUtil.rgb(252, 157, 3));
        listOne.add(ColorUtil.rgb(3, 252, 173));
        listOne.add(ColorUtil.rgb(252, 3, 119));
        listOne.add(ColorUtil.rgb(194, 252, 3));
        listOne.add(ColorUtil.rgb(252, 3, 123));
        listOne.add(ColorUtil.rgb(182, 252, 3));
        listOne.add(ColorUtil.rgb(111, 3, 252));
        listOne.add(ColorUtil.rgb(3, 202, 252));

        listTwo.add(ColorUtil.rgb(3, 202, 252));
        listTwo.add(ColorUtil.rgb(111, 3, 252));
        listTwo.add(ColorUtil.rgb(182, 252, 3));
        listTwo.add(ColorUtil.rgb(252, 3, 123));
        listTwo.add(ColorUtil.rgb(194, 252, 3));
        listTwo.add(ColorUtil.rgb(252, 3, 119));
        listTwo.add(ColorUtil.rgb(3, 252, 173));
        listTwo.add(ColorUtil.rgb(252, 157, 3));
        listTwo.add(ColorUtil.rgb(3, 236, 252));
        listTwo.add(ColorUtil.rgb(194, 3, 252));
        listTwo.add(ColorUtil.rgb(3, 252, 23));
        listTwo.add(ColorUtil.rgb(252, 219, 3));

    }

}
