package com.falcontechnology.statics;

import com.codename1.charts.util.ColorUtil;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.spinner.Picker;
import com.codename1.util.regex.RE;
import static com.falcontechnology.MyApplication.materialIcon;
import java.util.Date;

public class FormFiled {

    ///what field
    private Boolean isTextField = Boolean.TRUE;
    private Boolean isPicker = Boolean.FALSE;

    ///components
    private TextField field = new TextField();
    private Picker picker = new Picker();
    private Label fieldName = new Label();
    private String hintLabel = "";
    private Label errorMessage = new Label();
    private Container box = new Container(BoxLayout.y());
    private Container insideBox = new Container(BoxLayout.y());
    private Container textFieldBox = new Container(new BorderLayout());
    private Container buttonsBox = new Container(BoxLayout.x());

    ///data
    private String[] pickerData;
    private int pickerType;

    ///validator types
    private Boolean isNumeric = Boolean.FALSE;
    private Boolean isEmail = Boolean.FALSE;
    private Boolean isPhone = Boolean.FALSE;
    private Boolean idMandatory = Boolean.FALSE;
    private Boolean isPassword = Boolean.FALSE;
    private Boolean showPromptText = Boolean.FALSE;
    private Boolean isEditable = Boolean.TRUE;

    ///commands
    private Boolean isBlueBackground = Boolean.FALSE;

    ///other support
    /**
     * create a form field - TextField with label
     *
     * @param Boolean isBlueBackground
     * @param String fieldLabel
     * @return
     */
    public FormFiled(Boolean isBlueBackground, String fieldLabel) {
        this.isBlueBackground = isBlueBackground;
        fieldName.setText(fieldLabel);
        initComponent();
    }

    /**
     * create a form field - TextField with no label but prompt text
     *
     * @param Boolean isBlueBackground
     * @param String fieldLabel
     * @return
     */
    public FormFiled(Boolean isBlueBackground, String fieldLabel, Boolean showPromptText) {
        this.isBlueBackground = isBlueBackground;
        this.showPromptText = showPromptText;
        this.hintLabel = fieldLabel;
        fieldName.setText(fieldLabel);
        initComponent();
    }

    /**
     * create a form field that is of type picker
     *
     * @param Boolean isBlueBackground
     * @param String fieldLabel
     * @param Boolean isPicker
     * @param int pickerType
     * @return
     */
    public FormFiled(Boolean isBlueBackground, String fieldLabel, Boolean isPicker, int pickerType) {
        this.isBlueBackground = isBlueBackground;
        this.isPicker = isPicker;
        this.pickerType = pickerType;
        fieldName.setText(fieldLabel);
        initComponent();
    }

    private void initComponent() {

        textFieldBox.getStyle().setMargin(0, 0, 0, 0);
        textFieldBox.getStyle().setMarginUnit(Style.UNIT_TYPE_DIPS);
        textFieldBox.getStyle().setPadding(0, 0, 0, 0);
        textFieldBox.getStyle().setPaddingUnit(Style.UNIT_TYPE_DIPS);

        if (isTextField) {
            textFieldBox.add(BorderLayout.CENTER, field);
        }
        if (isPicker) {
            picker.setType(this.pickerType);
            textFieldBox.add(BorderLayout.CENTER, picker);
        }
        insideBox.add(textFieldBox);
        if (!showPromptText) {
            box.add(fieldName);
        } else {
            field.setHint(hintLabel);

        }
        box.add(insideBox);

        StyleMaker();

        field.addDataChangedListener((i1, i2) -> {
            if (isNumeric) {
                field.setConstraint(TextField.NUMERIC);
                NumericValidators();
            } else if (isEmail) {
                EmailValidators();
            } else if (isPhone) {
                PhoneValidators();
            }
        });
    }

    boolean isView = false;

    private void initPassword() {

        Button eye = new Button();
        if (isBlueBackground) {
            eye.setIcon(materialIcon(FontImage.MATERIAL_VISIBILITY_OFF, 3, 0xffffff));
        } else {
            eye.setIcon(materialIcon(FontImage.MATERIAL_VISIBILITY_OFF, 3, ColorUtil.rgb(117, 117, 117)));
        }
        eye.setUIID("eyebluff");
        eye.getStyle().setMargin(0, 0, 0, 0);
        eye.getStyle().setMarginUnit(Style.UNIT_TYPE_DIPS);
        eye.getStyle().setPadding(0, 0, 0, 0);
        eye.getStyle().setPaddingUnit(Style.UNIT_TYPE_DIPS);
        if (textFieldBox.getComponentCount() == 1) {
            textFieldBox.add(BorderLayout.EAST, eye);
        }

        eye.addActionListener(e -> {

            if (isView) {
                field.setConstraint(TextField.PASSWORD);
                isView = false;
                if (isBlueBackground) {
                    eye.setIcon(materialIcon(FontImage.MATERIAL_VISIBILITY_OFF, 3, 0xffffff));
                } else {
                    eye.setIcon(materialIcon(FontImage.MATERIAL_VISIBILITY_OFF, 3, ColorUtil.rgb(117, 117, 117)));
                }
            } else {
                field.setConstraint(TextField.ANY);
                isView = true;
                if (isBlueBackground) {
                    eye.setIcon(materialIcon(FontImage.MATERIAL_VISIBILITY, 3, 0xffffff));
                } else {
                    eye.setIcon(materialIcon(FontImage.MATERIAL_VISIBILITY, 3, ColorUtil.rgb(117, 117, 117)));
                }
            }

            try {
                textFieldBox.getComponentForm().refreshTheme();
            } catch (Exception x) {
            }

        });

    }

    ///numeric validators
    private void NumericValidators() {
        try {
            StyleMaker_BeforeError();
            Double.parseDouble(field.getText());
            if (insideBox.getComponentCount() == 2) {
                insideBox.removeComponent(errorMessage);
                try {
                    insideBox.getComponentForm().refreshTheme();
                } catch (Exception x) {
                }
            }
        } catch (Exception e) {
            StyleMaker_AfterError();
            errorMessage.setText("Input must be a valid number!");
            if (insideBox.getComponentCount() == 1) {
                insideBox.add(errorMessage);
                try {
                    insideBox.getComponentForm().refreshTheme();
                } catch (Exception x) {
                }
            }

        }
    }

    private void EmailValidators() {

        RE pattern = new RE("^([a-zA-Z0-9.!#$%&'*+/=?^`{|}~]|-|_)+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
        if (pattern.match(field.getText())) {
            StyleMaker_BeforeError();
            if (insideBox.getComponentCount() == 2) {
                insideBox.removeComponent(errorMessage);
            }
            try {
                insideBox.getComponentForm().refreshTheme();
            } catch (Exception x) {
            }
        } else {
            StyleMaker_AfterError();
            errorMessage.setText("Input must be a valid Email!");
            if (insideBox.getComponentCount() == 1) {
                insideBox.add(errorMessage);
            }
            try {
                insideBox.getComponentForm().refreshTheme();
            } catch (Exception x) {
            }

        }
    }

    private void PhoneValidators() {

        String phoneRegEx = "^\\(?([0-9]{2})\\)?([ .-]?)([0-9]{3})\\2([0-9]{4})";

        RE pattern = new RE(phoneRegEx);
        if (pattern.match(field.getText())) {
            StyleMaker_BeforeError();
            if (insideBox.getComponentCount() == 2) {
                insideBox.removeComponent(errorMessage);
                try {
                    insideBox.getComponentForm().refreshTheme();
                } catch (Exception x) {
                }
            }
        } else {
            StyleMaker_AfterError();
            errorMessage.setText("Input must be a valid Phone No.!");
            if (insideBox.getComponentCount() == 1) {
                insideBox.add(errorMessage);
                try {
                    insideBox.getComponentForm().refreshTheme();
                } catch (Exception x) {
                }
            }

        }
    }

    ///styling
    private void StyleMaker() {

        if (isBlueBackground) {
            fieldName.setUIID("Commons-TextField-Labels-Blue");
            field.setUIID("Commons-TextField-BeforeError-Blue");
            picker.setUIID("Commons-TextField-BeforeError-Blue");
            errorMessage.setUIID("Commons-TextField-ValidationLabels-Blue");
            insideBox.setUIID("Commons-TextField-Box-Blue");
            if (showPromptText) {
                field.getHintLabel().setUIID("Commons-TextField-Labels-Blue");
            }
            if (!isEditable) {
                field.setUIID("Commons-TextField-Labels-Blue");
            }
        } else {
            fieldName.setUIID("Commons-TextField-Labels");
            field.setUIID("Commons-TextField-BeforeError");
            picker.setUIID("Commons-TextField-BeforeError");
            errorMessage.setUIID("Commons-TextField-ValidationLabels");
            insideBox.setUIID("Commons-TextField-Box");
            if (showPromptText) {
                field.getHintLabel().setUIID("Commons-TextField-Labels");
            }
            if (!isEditable) {
                field.setUIID("Commons-TextField-Labels");
            }
        }
    }

    private void StyleMaker_BeforeError() {
        if (isBlueBackground) {
            field.setUIID("Commons-TextField-BeforeError-Blue");
            picker.setUIID("Commons-TextField-BeforeError-Blue");
            if (!isEditable) {
                field.setUIID("Commons-TextField-Labels-Blue");
            }
        } else {
            field.setUIID("Commons-TextField-BeforeError");
            picker.setUIID("Commons-TextField-BeforeError");
            if (!isEditable) {
                field.setUIID("Commons-TextField-Labels");
            }
        }
    }

    private void StyleMaker_AfterError() {
        if (isBlueBackground) {
            field.setUIID("Commons-TextField-AfterError-Blue");
            picker.setUIID("Commons-TextField-AfterError-Blue");
        } else {
            field.setUIID("Commons-TextField-AfterError");
            picker.setUIID("Commons-TextField-AfterError");
        }
    }

    Button button = new Button();

    /**
     * Adds a button at the end of the textField to be used to make save or
     * update calls or any other calls. You can set add action to the button by
     * calling FormField.getButton.addAction.......
     *
     * @see getButton()
     *
     * @return void
     * @param
     */
    public void addButton(Image icon) {
        button.setUIID("eyebluff");
        button.getStyle().setMargin(0, 0, 0, 0);
        button.getStyle().setMarginUnit(Style.UNIT_TYPE_DIPS);
        button.getStyle().setPadding(0, 0, 0, 0);
        button.getStyle().setPaddingUnit(Style.UNIT_TYPE_DIPS);
        button.setIcon(icon);

        buttonsBox.add(button);

        if (textFieldBox.getComponentCount() == 1) {
            textFieldBox.add(BorderLayout.EAST, buttonsBox);
        }

        try {
            textFieldBox.getComponentForm().refreshTheme();
        } catch (Exception x) {
        }
    }

    /**
     * returns the button so you can set actions on it
     *
     * @return Button
     * @param
     */
    public Button getButton() {
        return this.button;
    }

    Button button2 = new Button();

    /**
     * Adds second button at the end of the textField to be used to make save or
     * update calls or any other calls. You can set add action to the button by
     * calling FormField.getButton.addAction.......
     *
     * @see getButton1(), getButton2()
     *
     * @return void
     * @param
     */
    public void addSecondButton(Image icon) {
        button2.setUIID("eyebluff");
        button2.getStyle().setMargin(0, 0, 3, 0);
        button2.getStyle().setMarginUnit(Style.UNIT_TYPE_DIPS);
        button2.getStyle().setPadding(0, 0, 0, 0);
        button2.getStyle().setPaddingUnit(Style.UNIT_TYPE_DIPS);

        button2.setIcon(icon);

        buttonsBox.add(button2);

        if (textFieldBox.getComponentCount() == 1) {
            textFieldBox.add(BorderLayout.EAST, buttonsBox);
        }

        try {
            textFieldBox.getComponentForm().refreshTheme();
        } catch (Exception x) {
        }
    }

    /**
     * returns the second button so you can set actions on it
     *
     * @return Second Button
     * @param
     */
    public Button getSecondButton() {
        return this.button2;
    }

    /**
     * Works only if the field is TextField or is a Picker of type
     * Display.PICKER_TYPE_STRINGS
     *
     * @return String Text. [The string value of field.getText(). if type is
     * Picker, then this text is the value of picker.getSelectedString()]
     */
    public String getContent() {

        String xx = null;
        if (isTextField) {
            xx = field.getText();
        }

        if (isPicker && Display.PICKER_TYPE_STRINGS == this.pickerType && pickerData != null) {
            xx = picker.getSelectedString();
        }
        return xx;

    }

    /**
     * Works only if the field is a Picker of type
     * Display.PICKER_TYPE_DATE,Display.PICKER_TYPE_CALENDAR or
     * Display.PICKER_TYPE_DATE_AND_TIME
     *
     * @param formatter String formatter that is used to convert the value of
     * the selected Date to string eg "yyyy-MM-dd"
     * @return String DateText. [the formatted Date from (new
     * SimpleDateFormat(formatter)).format(this.picker.getDate())]
     */
    public String getDateContent() {

        if (isPicker) {
            if (Display.PICKER_TYPE_DATE == this.pickerType
                    || Display.PICKER_TYPE_CALENDAR == this.pickerType
                    || Display.PICKER_TYPE_DATE_AND_TIME == this.pickerType) {

                return (new SimpleDateFormat("dd/MM/yyyy")).format(this.picker.getDate());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public String convertDate() {
        if (isPicker) {
            if (Display.PICKER_TYPE_DATE == this.pickerType
                    || Display.PICKER_TYPE_CALENDAR == this.pickerType
                    || Display.PICKER_TYPE_DATE_AND_TIME == this.pickerType) {

                return (new SimpleDateFormat("yyyy-MM-dd")).format(this.picker.getDate());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Works only if the field is a Picker of type Display.PICKER_TYPE_STRINGS
     * and the Picker must be set to data[]
     *
     * @param
     * @return int index. [The position of the selected string index from
     * picker.getSelectedStringIndex()]
     */
    public int getContentIndex() {

        if (isPicker && Display.PICKER_TYPE_STRINGS == this.pickerType && pickerData != null) {
            return this.picker.getSelectedStringIndex();
        } else {
            return -1;
        }
    }

    /**
     * Set the Label/Name of the field
     *
     * @param fieldName of String Type
     * @return void picker.getSelectedStringIndex()]
     */
    public void setFieldName(String fieldName) {
        this.fieldName.setText(fieldName);
        try {
            box.getComponentForm().refreshTheme();
        } catch (Exception e) {
        }
    }

    /**
     * Set the field to readOnly
     *
     * @param
     * @return
     */
    public void setEditable(Boolean editable) {
        isEditable = editable;
        field.setEditable(isEditable);
    }

    /**
     * Set Hint Label,Image
     *
     * @param String hintText
     * @param Image icon
     * @return
     */
    public void setHintLabel(String hintText, Image icon) {

        field.setHint(hintText, icon);
    }

    public TextField getField() {
        return field;
    }

    public Label getFieldName() {
        return fieldName;
    }

    public Label getErrorMessage() {
        return errorMessage;
    }

    public Container getFormField() {
        return box;
    }

    public void setIsPhone(Boolean isPhone) {
        this.isPhone = isPhone;
    }

    public void setIsNumeric(Boolean isNumeric) {
        this.isNumeric = isNumeric;
    }

    public void setIsEmail(Boolean isEmail) {
        this.isEmail = isEmail;
    }

    public void setIdMandatory(Boolean idMandatory) {
        this.idMandatory = idMandatory;
    }

    public void setIsBlueBackground(Boolean isBlueBackground) {
        this.isBlueBackground = isBlueBackground;
    }

    public void setIsPassword(Boolean isPassword) {
        if (isPassword) {
            field.setConstraint(TextField.PASSWORD);
            initPassword();
        }
        this.isPassword = isPassword;
    }

    public void setContentBeforeShow(String text) {
        this.field.setText(text);

        try {
            textFieldBox.getComponentForm().refreshTheme();
        } catch (Exception e) {
        }

    }

    public void setIsTextField(Boolean isTextField) {
        this.isTextField = isTextField;
    }

    public void setIsPicker(Boolean isPicker) {
        this.isPicker = isPicker;
    }

    public Picker getPicker() {
        return picker;
    }

    public void setToDateType(int type) {
        this.picker.setType(type);
        try {
            textFieldBox.getComponentForm().refreshTheme();
        } catch (Exception e) {
        }
    }

    public void setPickerData(String[] pickerData) {
        this.pickerData = pickerData;
        this.picker.setStrings(pickerData);
        try {
            textFieldBox.getComponentForm().refreshTheme();
        } catch (Exception e) {
        }
    }

    public void setPickerDate(String date) {
        if (isPicker && (Display.PICKER_TYPE_DATE == this.pickerType
                || Display.PICKER_TYPE_CALENDAR == this.pickerType
                || Display.PICKER_TYPE_DATE_AND_TIME == this.pickerType)) {
            try {
                this.picker.setDate((new SimpleDateFormat("dd/MM/yyyy")).parse(date));
            } catch (ParseException ex) {
            }
            try {
                textFieldBox.getComponentForm().refreshTheme();
            } catch (Exception e) {
            }
        }
    }

    public void setPickerDate(Date date) {
        if (isPicker && (Display.PICKER_TYPE_DATE == this.pickerType
                && Display.PICKER_TYPE_CALENDAR == this.pickerType
                && Display.PICKER_TYPE_DATE_AND_TIME == this.pickerType)) {
            this.picker.setDate(date);
            try {
                textFieldBox.getComponentForm().refreshTheme();
            } catch (Exception e) {
            }
        }
    }

    public void setSelectedPickerString(String text) {

        try {
            this.picker.setSelectedString(text);
        } catch (Exception e) {
        }

        try {
            textFieldBox.getComponentForm().refreshTheme();
        } catch (Exception e) {
        }
    }

    public void setSelectedPickerIndex(int index) {

        try {
            this.picker.setSelectedStringIndex(index);
        } catch (Exception e) {
        }

        try {
            textFieldBox.getComponentForm().refreshTheme();
        } catch (Exception e) {
        }
    }

    public int getPickerType() {
        return pickerType;
    }
}
